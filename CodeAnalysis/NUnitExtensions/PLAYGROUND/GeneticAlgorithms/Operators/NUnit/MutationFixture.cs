﻿using System.Linq;
using Fugro.Collections;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Operators.NUnit
{
    [TestFixture]
    internal sealed class MutationFixture
    {
        [Test]
        public void BLXAlphaMutationReturnsGenomesInExpectedRange()
        {
            const double min = 0;
            const double max = 2;
            const double alpha = 0.3;
            const double mutationProbability = 1;

            ImmutableList<double> genome = Enumerable.Repeat(1d, 10).ToImmutableList();
            ImmutableList<double> minRanges = Enumerable.Repeat(min, 10).ToImmutableList();
            ImmutableList<double> maxRanges = Enumerable.Repeat(max, 10).ToImmutableList();

            const double expectedMin = min - (max - min) * alpha;
            const double expectedMax = max + (max - min) * alpha;

            for (var i = 0; i < 1000; i++)
            {
                ImmutableList<double> mutatedGenome = Mutation.BLXAlphaMultipointMutation(genome, minRanges, maxRanges, alpha, mutationProbability);
                foreach (var gene in mutatedGenome)
                {
                    Assert.That(gene, Is.InRange(expectedMin, expectedMax));
                }
            }
        }

    }
}
