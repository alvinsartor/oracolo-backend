﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Components;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Operators.NUnit
{
    [TestFixture]
    internal sealed class SelectionFixture
    {
        private const int s_NrExecutions = 1000;
        private Configuration m_Configuration;

        [TestFixtureSetUp]
        public void SetUp()
        {
            m_Configuration = Configuration.Default();
        }

        [Test]
        public void RouletteSelectionThrowsAnErrorIfTheFitnessesAreNegative()
        {
            ImmutableList<string> individuals = new[] { "foo", "bar", "foobar" }.ToImmutableList();
            double[] fitnesses = { 1, 2, -1 };

            Assert.Throws<InvalidOperationException>(() => Selection.RouletteSelection(individuals, fitnesses, true, 4));
        }

        [Test]
        public void RouletteSelectionThrowsAnErrorIfItIsAMinimizationProblem()
        {
            ImmutableList<string> individuals = new[] { "foo", "bar", "foobar" }.ToImmutableList();
            double[] fitnesses = { 1, 2, 1 };

            Assert.Throws<InvalidOperationException>(() => Selection.RouletteSelection(individuals, fitnesses, false, 4));
        }

        [Test]
        public void CongruenceTestRoulette()
        {
            ImmutableList<string> individuals = new[] { "fit_1", "fit_2", "fit_3", "fit_4" }.ToImmutableList();
            double[] fitnesses = { 1, 2, 3, 4 };

            Queue<string> selected = Selection.RouletteSelection(individuals, fitnesses, true, s_NrExecutions);

            CountAndPrint(selected);
        }

        [Test]
        public void CongruenceTestRanking()
        {
            ImmutableList<string> individuals = new[] { "fit_1", "fit_2", "fit_3", "fit_4" }.ToImmutableList();
            double[] fitnesses = { 1, 2, 3, 4 };

            Queue<string> selected =
                Selection.RankingSelection(individuals, fitnesses, true, s_NrExecutions, m_Configuration.GeneticPressureForRankingSelection);

            CountAndPrint(selected);
        }

        [Test]
        public void CongruenceTestRankingNegative()
        {
            ImmutableList<string> individuals = new[] { "fit_1", "fit_2", "fit_3", "fit_4" }.ToImmutableList();
            double[] fitnesses = { 1, 2, 3, 4 };

            Queue<string> selected =
                Selection.RankingSelection(individuals, fitnesses, false, s_NrExecutions, m_Configuration.GeneticPressureForRankingSelection);

            CountAndPrint(selected);
        }

        [Test]
        public void CongruenceTestTournament()
        {
            ImmutableList<string> individuals = new[] { "fit_1", "fit_2", "fit_3", "fit_4" }.ToImmutableList();
            double[] fitnesses = { 1, 2, 3, 4 };

            Queue<string> selected = Selection.RankingSelection(individuals, fitnesses, true, s_NrExecutions, m_Configuration.TournamentSize);

            CountAndPrint(selected);
        }

        [Test]
        public void CongruenceTestTournamentNegative()
        {
            ImmutableList<string> individuals = new[] { "fit_1", "fit_2", "fit_3", "fit_4" }.ToImmutableList();
            double[] fitnesses = { 1, 2, 3, 4 };

            Queue<string> selected = Selection.RankingSelection(individuals, fitnesses, false, s_NrExecutions, m_Configuration.TournamentSize);

            CountAndPrint(selected);
        }

        private static void CountAndPrint([NotNull] IEnumerable<string> selected)
        {
            IGrouping<string, string>[] groups = selected.GroupBy(x => x).ToArray();
            string[] keys = groups.Select(x => x.Key).ToArray();
            Array.Sort(keys, groups);

            Console.WriteLine(@"Number selected individuals " + s_NrExecutions);
            Console.WriteLine();

            string results = groups.Aggregate("appearances: \n",
                (current, group) => current + (group.Key + "-> " + group.Count() + "\n"));

            Console.WriteLine(results);
        }

    }
}
