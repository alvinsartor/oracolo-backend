﻿using System;
using NUnit.Framework;

namespace Extensions.NUnit
{
    [TestFixture]
    internal sealed class NullCheckFixture
    {
        [Test]
        public void AssertNotNullThrowsWhenNull()
        {            
            var exception = Assert.Throws<ArgumentException>(() => ((object) null).AssertNotNull("Variable"));
            Assert.That(exception.Message, Is.EqualTo("Variable is null."));

            Assert.DoesNotThrow(() => new object().AssertNotNull("Object"));            
        }

        [Test]
        public void NotNullThrowsWhenNull()
        {
            object variable = null;

            var exception = Assert.Throws<ArgumentException>(() => variable = ((object)null).NotNull("Variable"));
            Assert.That(exception.Message, Is.EqualTo("Variable is null."));
            Assert.Null(variable);

            Assert.DoesNotThrow(() => variable = new object().NotNull("Object"));
            Assert.NotNull(variable);
        }
    }
}
