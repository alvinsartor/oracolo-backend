﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using NUnit.Framework;
using Random = Fugro.GeneticAlgorithms.Util.Random;

namespace Fugro.GeneticAlgorithms.Operators.NUnit
{
    [TestFixture]
    internal sealed class ReplacementFixture
    {
        // Generational replacement tests

        [Test]
        public void WhenReplacementIsGenerationalTheNewPopulationSizeHasToBeEqualToTheLastPopulation()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, 3 }.ToImmutableList();
            ImmutableList<int> sameSizePopulation = new[] { 4, 5, 6 }.ToImmutableList();
            ImmutableList<int> smallerPopulation = new[] { 4, 5, }.ToImmutableList();
            ImmutableList<int> biggerPopulation = new[] { 4, 5, 6, 7 }.ToImmutableList();
            IList<int> newPopulation = null;

            Assert.Throws<InvalidOperationException>(() => Replacement.GenerationalReplacement(oldPopulation, smallerPopulation));
            Assert.Throws<InvalidOperationException>(() => Replacement.GenerationalReplacement(oldPopulation, biggerPopulation));

            Assert.DoesNotThrow(() => newPopulation = Replacement.GenerationalReplacement(oldPopulation, sameSizePopulation));
            Assert.NotNull(newPopulation);
            Assert.That(newPopulation, Is.EqualTo(sameSizePopulation));
        }

        // Less-Fit replacement tests

        [Test]
        public void LessFitReplacementReplacesTheLessFitIndividuals()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, 3, 4 }.ToImmutableList();
            var fitnesses = new[] { 0.1, 0.2, 0.3, 0.4 };
            ImmutableList<int> newBorns = new[] { 5, 6, 7 }.ToImmutableList();

            ImmutableList<int> expectedPopulation = new[] { 5, 6, 7, 4 }.ToImmutableList();
            IList<int> newPopulation = Replacement.LessFitReplacement(oldPopulation, fitnesses, newBorns);

            Assert.NotNull(newPopulation);
            Assert.True(expectedPopulation.All(x => newPopulation.Contains(x)));
        }

        [Test]
        public void LessFitReplacementThrowsIfNewBornsAreMoreThanOldPopulation()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, }.ToImmutableList();
            var fitnesses = new[] { 0.1, 0.2 };
            ImmutableList<int> newBorns = new[] { 5, 6, 7 }.ToImmutableList();

            Assert.Throws<InvalidOperationException>(() => Replacement.LessFitReplacement(oldPopulation, fitnesses, newBorns));
        }

        [Test]
        public void LessFitReplacementThrowsIfFitnessesNumberDoesntMatchWithPopulation()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, 3 }.ToImmutableList();
            var fitnesses = new[] { 0.1, 0.2 };
            ImmutableList<int> newBorns = new[] { 5, 6, 7 }.ToImmutableList();

            Assert.Throws<InvalidOperationException>(() => Replacement.LessFitReplacement(oldPopulation, fitnesses, newBorns));
        }

        [Test]
        public void LessFitReplacementReturnsAPopulationOfTheSameSizeAsTheOldOne()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, 3, 4, 5 }.ToImmutableList();
            var fitnesses = new[] { 0.1, 0.2, 0.3, 0.4, 0.5 };
            ImmutableList<int> newBorns = new[] { 5, 6 }.ToImmutableList();

            ImmutableList<int> newPopulation = Replacement.LessFitReplacement(oldPopulation, fitnesses, newBorns);
            Assert.That(newPopulation.Count, Is.EqualTo(oldPopulation.Count));
        }

        // Random replacement tests

        [Test]
        public void RandomReplacementThrowsIfNewBornsAreMoreThanOldPopulation()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, }.ToImmutableList();
            ImmutableList<int> newBorns = new[] { 5, 6, 7 }.ToImmutableList();

            Assert.Throws<InvalidOperationException>(() => Replacement.RandomReplacement(oldPopulation, newBorns));
        }

        [Test]
        public void RandomReplacementReturnsAPopulationOfTheSameSizeAsTheOldOne()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, 3, 4, 5 }.ToImmutableList();
            ImmutableList<int> newBorns = new[] { 5, 6 }.ToImmutableList();

            ImmutableList<int> newPopulation = Replacement.RandomReplacement(oldPopulation, newBorns);
            Assert.That(newPopulation.Count, Is.EqualTo(oldPopulation.Count));
        }

        // Most-Similar replacement tests

        private static double DistanceFunction(int i0, int i1) => Math.Abs(Math.Floor(Math.Sqrt(i0)) - Math.Floor(Math.Sqrt(i1)));

        [Test]
        public void MostSimilarReplacementReplacesTheMostSimilarIndividuals()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 4, 9, 26 }.ToImmutableList();
            ImmutableList<int> newBorns = new[] { 2, 7, 27 }.ToImmutableList();

            ImmutableList<int> expectedPopulation = new[] { 2, 7, 27, 9 }.ToImmutableList();

            IList<int> newPopulation = Replacement.MostSimilarReplacement(oldPopulation, newBorns, DistanceFunction);

            Assert.NotNull(newPopulation);
            Assert.True(expectedPopulation.All(x => newPopulation.Contains(x)));
        }

        [Test]
        public void MostSimilarReplacementThrowsIfNewBornsAreMoreThanOldPopulation()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, }.ToImmutableList();
            ImmutableList<int> newBorns = new[] { 5, 6, 7 }.ToImmutableList();

            double DistanceFunction(int i0, int i1) => Random.GetRandomInRange(0, 10);

            Assert.Throws<InvalidOperationException>(() => Replacement.MostSimilarReplacement(oldPopulation, newBorns, DistanceFunction));
        }

        [Test]
        public void MostSimilarReplacementReturnsAPopulationOfTheSameSizeAsTheOldOne()
        {
            ImmutableList<int> oldPopulation = new[] { 1, 2, 3, 4, 5 }.ToImmutableList();
            ImmutableList<int> newBorns = new[] { 5, 6 }.ToImmutableList();

            ImmutableList<int> newPopulation = Replacement.MostSimilarReplacement(oldPopulation, newBorns, DistanceFunction);
            Assert.That(newPopulation.Count, Is.EqualTo(oldPopulation.Count));
        }

    }
}
