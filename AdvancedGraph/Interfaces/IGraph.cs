﻿using System;
using System.Collections.Immutable;
using JetBrains.Annotations;

namespace AdvancedGraph.Interfaces
{
    public interface IGraph<T> : IEquatable<IGraph<T>>
    {
        /// <summary>
        /// Gets the graph name.
        /// </summary>
        [NotNull] string Name { get; }

        /// <summary>
        /// Gets the graph identifier.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// The number of nodes in the graph.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets the nodes of this graph.
        /// </summary>
        [NotNull] ImmutableHashSet<INode<T>> Nodes { get; }

        /// <summary>
        /// Gets all the values of the nodes of this graph.
        /// </summary>
        [NotNull] ImmutableHashSet<T> Values { get; }

        /// <summary>
        /// Gets the <see cref="Node{T}"/> with the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <exception cref="ArgumentException">Value not in graph.</exception>
        [NotNull] INode<T> this[T value] { get; }

        /// <summary>
        /// Determines whether the graph contains a node with the specified value.
        /// </summary>
        /// <param name="value">The value of the searched node.</param>
        bool Contains([NotNull] T value);

        /// <summary>
        /// Determines whether the graph contains the specified node.
        /// </summary>
        /// <param name="valueId">The searched node.</param>
        bool Contains(Guid valueId);

        /// <summary>
        /// Determines whether this graph has cycles.
        /// </summary>
        /// <returns>
        ///   <c>true</c> a cyclic connection is spotted; otherwise, <c>false</c>.
        /// </returns>
        bool HasCycles();

        /// <summary>
        /// Occurs when a node is added to the graph.
        /// </summary>
        event EventHandler<T> OnNodeAdded;

        /// <summary>
        /// Occurs when a node is removed from the graph.
        /// </summary>
        event EventHandler<T> OnNodeRemoved;
    }
}
