﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components
{
    internal sealed class PopulationArchive<T>
    {
        [NotNull] private readonly Func<Individual<T>, Individual<T>, double> m_CalculateGeneticDiversity;
        [NotNull] private readonly Configuration m_Configuration;

        private ImmutableList<IndividualsWithFitness<T>> m_Archive;
        private readonly LinkedList<GenerationStatistics<T>> m_Statistics;

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulationArchive{T}"/> class.
        /// </summary>
        public PopulationArchive([NotNull] Configuration configuration,
            [NotNull] Func<Individual<T>, Individual<T>, double> calculateGeneticDiversity)
        {
            m_CalculateGeneticDiversity = calculateGeneticDiversity.NotNull(nameof(calculateGeneticDiversity));
            m_Configuration = configuration.NotNull(nameof(configuration));

            m_Archive = ImmutableList<IndividualsWithFitness<T>>.Empty;
            m_Statistics = new LinkedList<GenerationStatistics<T>>();
        }

        /// <summary>
        /// Gets the archive.
        /// </summary>
        [NotNull] public ImmutableList<IndividualsWithFitness<T>> Archive => m_Archive;

        [NotNull] public ImmutableList<GenerationStatistics<T>> Statistics => m_Statistics.ToImmutableList();

        /// <summary>
        /// Updates the archive and the statistics with a new generation of individuals.
        /// </summary>
        /// <param name="individuals">The individuals.</param>
        public void Update([NotNull] ImmutableList<IndividualsWithFitness<T>> individuals)
        {
            ArgumentHelper.AssertNotNull(individuals, nameof(individuals));

            UpdateArchive(individuals);
            UpdateStatistics(individuals);
        }

        private void UpdateStatistics([NotNull] ImmutableList<IndividualsWithFitness<T>> individuals)
        {
            GenerationStatistics<T> lastStatistic = m_Statistics.LastOrDefault();
            var statistic = new GenerationStatistics<T>(individuals, m_Configuration, lastStatistic, m_CalculateGeneticDiversity);
            m_Statistics.AddLast(statistic);
        }

        private void UpdateArchive([NotNull] IEnumerable<IndividualsWithFitness<T>> individuals)
        {
            ImmutableList<IndividualsWithFitness<T>>
                newArchive = m_Archive.Union(individuals).OrderByDescending(x => x.GetFitness()).ToImmutableList();

            m_Archive = newArchive.Count > m_Configuration.PopulationArchiveSize
                ? newArchive.WithRemoveRange(m_Configuration.PopulationArchiveSize, newArchive.Count - m_Configuration.PopulationArchiveSize)
                : newArchive;
        }
    }
}
