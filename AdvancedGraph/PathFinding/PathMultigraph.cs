﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph.PathFinding
{
    public sealed class PathMultigraph<T> : Path<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathMultigraph{T}"/> class.
        /// </summary>
        /// <remarks>Use this constructor to build a path in a multigraph.</remarks>
        /// <param name="multigraph">The multigraph.</param>
        /// <param name="parents">A dictionary containing the parents of the nodes.</param>
        /// <param name="destination">The destination node.</param>
        internal PathMultigraph(
            [NotNull] IReadOnlyList<IGraph<T>> multigraph,
            [NotNull] IReadOnlyDictionary<INode<T>, INode<T>> parents,
            [NotNull] INode<T> destination)
        {
            var result = new List<IEdge<T>>();
            INode<T> actual = destination;
            INode<T> previous = parents[actual];

            while (previous != null)
            {
                var edge = GetEdgeFromMultigraph(multigraph, previous.Value, actual.Value);
                result.Add(edge);

                actual = previous;
                previous = parents[actual];
            }

            Edges = result.ToImmutableList().Reverse();
        }

        /// <summary>
        /// Gets the edge with the lowest weight that connects the specified values from the multigraph.
        /// </summary>
        /// <param name="multigraph">The multigraph.</param>
        /// <param name="startValue">The start value.</param>
        /// <param name="destinationValue">The destination value.</param>
        [NotNull]
        private static IEdge<T> GetEdgeFromMultigraph([NotNull] IReadOnlyList<IGraph<T>> multigraph, T startValue, T destinationValue)
        {
            var edges = multigraph
                .Where(graph => graph.Contains(startValue) && graph[startValue].IsConnectedTo(destinationValue))
                .Select(graph => graph[startValue].Edges[destinationValue])
                .ToList();

            if (edges.Count == 0)
            {
                throw new ArgumentException("No edge found between start and destination in the multigraph.");
            }

            return edges.MinBy(edge => edge.Weight);
        }
    }
}