using System;
using System.Collections.Immutable;
using System.IO;
using CodeAnalysis.Mapper.Helpers.NUnitExtensions;
using NUnit.Framework;

namespace CodeAnalysis.Mapper.Helpers.NUnit
{
    [TestFixture]
    internal sealed class CsprojAnaliserFixture
    {
        private TemporaryCsprojFile m_TemporaryCsproj;

        [OneTimeSetUp]
        public void SetUp()
        {
            m_TemporaryCsproj = new TemporaryCsprojFile();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            m_TemporaryCsproj.Dispose();
        }


        [Test]
        public void AnaliserThrowsIfFileIsNotACsproj()
        {
            string path = Path.GetTempFileName();
            Assert.Throws<ArgumentException>(() => CsprojAnaliser.GetOutputPathsFromCsproj(path, "AdvancedGraph"));
            Assert.Throws<ArgumentException>(() => CsprojAnaliser.GetAssembliesPathsFromCsproj(path));
            Assert.Throws<ArgumentException>(() => CsprojAnaliser.GetAllCSharpFilePathsFromCsproj(path));
            File.Delete(path);
        }

        [Test]
        public void AnaliserThrowsIfFileDoesNotExist()
        {
            Assert.Throws<FileNotFoundException>(() => CsprojAnaliser.GetOutputPathsFromCsproj("C:\\Whatever.csproj", "AdvancedGraph"));
            Assert.Throws<FileNotFoundException>(() => CsprojAnaliser.GetAssembliesPathsFromCsproj("C:\\Whatever.csproj"));
            Assert.Throws<FileNotFoundException>(() => CsprojAnaliser.GetAllCSharpFilePathsFromCsproj("C:\\Whatever.csproj"));
        }

        [Test]
        public void AnaliserHasAccessToOutputPath()
        {
            string outputPath = CsprojAnaliser.GetOutputPathsFromCsproj(m_TemporaryCsproj.FilePath, "AdvancedGraph");
            string expectedPath = Path.Combine(Path.GetDirectoryName(m_TemporaryCsproj.FilePath) ?? "", "bin", "Debug", "AdvancedGraph.dll");
            Assert.That(outputPath, Is.EqualTo(expectedPath));
        }

        [Test]
        public void CsprojDependenciesAreFetched()
        {
            string folder = Path.GetDirectoryName(Path.GetDirectoryName(m_TemporaryCsproj.FilePath)) ?? "";
            var expectedDependencies = new[]
            {
                new DllNameAndPath("JetBrains.Annotations", 
                                   Path.Combine(folder, @"packages\JetBrains.Annotations.2018.2.1\lib\net20\JetBrains.Annotations.dll")),
                new DllNameAndPath("nunit.framework",
                                   Path.Combine(folder, @"packages\NUnit.3.11.0\lib\net45\nunit.framework.dll")),
                new DllNameAndPath("System.Collections.Immutable",
                                   Path.Combine(folder, @"packages\System.Collections.Immutable.1.5.0\lib\netstandard2.0\System.Collections.Immutable.dll")),
                new DllNameAndPath("Extensions",
                                   Path.Combine(folder, @"Extensions\Extensions.csproj")),
            };

            ImmutableList<DllNameAndPath> dependencies = CsprojAnaliser.GetAssembliesPathsFromCsproj(m_TemporaryCsproj.FilePath);
            CollectionAssert.AreEquivalent(expectedDependencies, dependencies);
        }

        [Test]
        public void FilesContainedInTheCsprojAreFetched()
        {
            string folder = Path.GetDirectoryName(m_TemporaryCsproj.FilePath) ?? "";
            var expectedFiles = new[]
            {
                Path.Combine(folder, "Edge.cs"),
                Path.Combine(folder, "DynamicGraph.cs"),
                Path.Combine(folder, "Graph.cs"),
                Path.Combine(folder, "Interfaces\\IEdge.cs"),
                Path.Combine(folder, "Interfaces\\IGraph.cs"),
                Path.Combine(folder, "Interfaces\\INode.cs"),
                Path.Combine(folder, "Interfaces\\IDynamicGraph.cs"),
                Path.Combine(folder, "Node.cs"),
                Path.Combine(folder, "NodeState.cs"),
                Path.Combine(folder, "NUnit\\DynamicGraphFixture.cs"),
                Path.Combine(folder, "NUnit\\EdgeFixture.cs"),
                Path.Combine(folder, "NUnit\\GraphFixture.cs"),
                Path.Combine(folder, "NUnit\\GenericGraphFixture.cs"),
                Path.Combine(folder, "NUnit\\NodeFixture.cs"),
                Path.Combine(folder, "PathFinding\\NUnit\\PathFindingMultigraphExtensionsFixture.cs"),
                Path.Combine(folder, "PathFinding\\NUnit\\PathFindingExtensionsFixture.cs"),
                Path.Combine(folder, "PathFinding\\NUnit\\PathMultigraphFixture.cs"),
                Path.Combine(folder, "PathFinding\\NUnit\\PathMonographFixture.cs"),
                Path.Combine(folder, "PathFinding\\NUnit\\PathFixture.cs"),
                Path.Combine(folder, "NUnit\\ToStringExtensionsFixture.cs"),
                Path.Combine(folder, "PathFinding\\PathMonograph.cs"),
                Path.Combine(folder, "PathFinding\\PathMultigraph.cs"),
                Path.Combine(folder, "PathFinding\\Path.cs"),
                Path.Combine(folder, "PathFinding\\PathFindingMultigraphExtensions.cs"),
                Path.Combine(folder, "PathFinding\\PathFindingMonographExtensions.cs"),
                Path.Combine(folder, "Properties\\AssemblyInfo.cs"),
                Path.Combine(folder, "ToStringExtensions.cs")
            };

            ImmutableList<string> csharpFiles = CsprojAnaliser.GetAllCSharpFilePathsFromCsproj(m_TemporaryCsproj.FilePath);
            CollectionAssert.AreEquivalent(expectedFiles, csharpFiles);
        }
    }
}