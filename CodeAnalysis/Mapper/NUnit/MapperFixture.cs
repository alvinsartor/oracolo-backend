using System.Linq;
using CodeAnalysis.ProjectMapping.Interfaces;
using NUnit.Framework;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper.NUnit
{
	[TestFixture]
	internal sealed class MapperFixture
	{
	    [NotNull] private static readonly string ProjectSlnPath = $@"C:\Users\a.sartor\source\repos\OrbWeaver\OrbWeaver.sln";

	    private Mapper m_Mapper;

        [OneTimeSetUp]
		public void SetUp()
		{
			m_Mapper = new Mapper(ProjectSlnPath);
		}       

		[Test]
		public void SlnIsCorrecltyMapped()
		{
			Assert.That(m_Mapper.SlnDescriptor.SlnName, Is.EqualTo("OrbWeaver"));
		}

	    [Test]
	    public void CsprojsAreCorrecltyMapped()
	    {
	        var expectedCsprojs = new[] {"Extensions", "AdvancedGraph", "CodeAnalysis"};
	        CollectionAssert.AreEquivalent(expectedCsprojs, m_Mapper.Csprojs.Values.Select(x => x.CsprojName));
	    }

        [Test]
        public void CsprojsDependanciesAreCorrecltyMapped()
        {
            var expectedCsprojs = new[] { "Extensions" };
            Assert.That(m_Mapper.DependenciesOf.Count, Is.EqualTo(m_Mapper.Csprojs.Count));

            ICsprojDescriptor extensions = m_Mapper.Csprojs.Values.Single(x => x.CsprojName == "Extensions");
            CollectionAssert.IsEmpty(m_Mapper.DependenciesOf[extensions].Select(x => x.CsprojName));

            ICsprojDescriptor advancedGraph = m_Mapper.Csprojs.Values.Single(x => x.CsprojName == "AdvancedGraph");
            CollectionAssert.AreEquivalent(expectedCsprojs, m_Mapper.DependenciesOf[advancedGraph].Select(x => x.CsprojName));

            ICsprojDescriptor codeAnalysis = m_Mapper.Csprojs.Values.Single(x => x.CsprojName == "CodeAnalysis");
            CollectionAssert.AreEquivalent(expectedCsprojs, m_Mapper.DependenciesOf[codeAnalysis].Select(x => x.CsprojName));
        }

	    [Test]
	    public void CsprojsInverseDependanciesAreCorrecltyMapped()
	    {
	        var expectedCsprojs = new[] { "AdvancedGraph", "CodeAnalysis" };
            Assert.That(m_Mapper.DependatsBy.Count, Is.EqualTo(m_Mapper.Csprojs.Count));

	        ICsprojDescriptor extensions = m_Mapper.Csprojs.Values.Single(x => x.CsprojName == "Extensions");
	        CollectionAssert.AreEquivalent(expectedCsprojs, m_Mapper.DependatsBy[extensions].Select(x => x.CsprojName));

	        ICsprojDescriptor advancedGraph = m_Mapper.Csprojs.Values.Single(x => x.CsprojName == "AdvancedGraph");
	        CollectionAssert.IsEmpty(m_Mapper.DependatsBy[advancedGraph].Select(x => x.CsprojName));

	        ICsprojDescriptor codeAnalysis = m_Mapper.Csprojs.Values.Single(x => x.CsprojName == "CodeAnalysis");
	        CollectionAssert.IsEmpty(m_Mapper.DependatsBy[codeAnalysis].Select(x => x.CsprojName));
        }
    }
}
