﻿using System;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper.Helpers
{
    public sealed class DllNameAndPath : IEquatable<DllNameAndPath>
    {
        /// <summary>
        /// Gets the csproj name.
        /// </summary>
        [NotNull] public string Name { get; }

        /// <summary>
        /// Gets the csproj path.
        /// </summary>
        [NotNull] public string Path { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DllNameAndPath"/> class.
        /// </summary>
        /// <param name="name">The name of the dll file.</param>
        /// <param name="path">The path of the dll file.</param>
        public DllNameAndPath([NotNull] string name, [NotNull] string path)
        {
            Name = name.NotNull(nameof(name));
            Path = path.NotNull(nameof(path));
        }

        /// <inheritdoc />
        public bool Equals(DllNameAndPath other)
        {
            return other != null
                   && other.Name.Equals(Name)
                   && other.Path.Equals(Path);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is DllNameAndPath dllNameAndPath
                   && dllNameAndPath.Equals(this);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return (Name.GetHashCode() * 397) ^ Path.GetHashCode();
            }
        }

        public override string ToString()
        {
            return $"{Name} - {Path}";
        }
    }
}
