﻿using System;
using NUnit.Framework;

namespace AdvancedGraph.NUnit
{
    [TestFixture]
    internal sealed class ToStringExtensionsFixture
    {
        [Test]
        public void ValuesRepresentationIsObtainedWhenToStringValueIsCalled()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Edge<int> edge = new Edge<int>(node42, node38, 5.0);

            const string expectedString = "'42' -> '38', [w:5]";

            Assert.That(edge.ToStringValues(i => i.ToString()), Is.EqualTo(expectedString));
            Console.WriteLine(edge.ToStringValues(i => i.ToString()));
        }


        [Test]
        public void ToStringAllowsToInsertAFunctionToPrintNodeContent()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);

            string expectedResult = $"Node ID: {node42.Id.ToString()}, Value: 52";

            Assert.That(node42.ToString(i => (i + 10).ToString()), Is.EqualTo(expectedResult));
            Console.WriteLine(node42.ToString(i => (i + 10).ToString()));
        }

        [Test]
        public void ToStringValueReturnsAStringRepresentationOfTheNodeAndItsConnections()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);
            Node<int> node12 = graph.Add(12);

            node42.AddConnection(node38, 10);
            node42.AddConnection(node12, 7);

            const string expectedConnection1 = " -> 12 [w:7] \n";
            const string expectedConnection2 = " -> 38 [w:10] \n";
            const string expectedNode = "Node: 42 \n";

            string result = node42.ToStringValues(i => i.ToString());

            Assert.That(result.Contains(expectedNode));
            Assert.That(result.Contains(expectedConnection1));
            Assert.That(result.Contains(expectedConnection2));

            Console.WriteLine(node42.ToStringValues(i => i.ToString()));
        }     
    }
}
