﻿using Extensions;
using JetBrains.Annotations;
using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using CodeAnalysis.Level1_Mining_DataStructure;
using CodeAnalysis.Level2_Extraction_DataStructure;

namespace CodeAnalysis.Level2_Extraction.Helpers
{
    internal static class EntityBuilder
    {
        [NotNull]
        public static IEnumerable<IEntity> BuildEntity([NotNull] string filePath, [NotNull] CompilationUnit compilationUnit)
        {
            filePath.AssertNotNull(nameof(filePath));
            compilationUnit.AssertNotNull(nameof(compilationUnit));

            //1. get necessary elements
            SyntaxTree syntaxTree = compilationUnit.GetSyntaxTreeOf(filePath);
            SemanticModel semanticModel = compilationUnit.GetSemanticModelOf(filePath);

            //2. get entities root
            var roots = SyntaxTreeAnaliser.GetEntitiesRoots(syntaxTree);

            //3. analise each entity
            return roots.Select(node => PerformAnalysis(node, semanticModel));
        }

        private static IEntity PerformAnalysis(SyntaxNode entityRootNode, SemanticModel model)
        {

        }

    }
}
