﻿using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Components.NUnit
{
    [TestFixture]
    internal sealed class PopulationArchiveFixture
    {
        private static double CalculateGeneticDiversity([NotNull] Individual<char> individual1, [NotNull] Individual<char> individual2) => 42;

        [NotNull, ItemNotNull]
        private static ImmutableList<IndividualsWithFitness<char>> GetIndividuals([NotNull] string genome, double fitnessValue)
        {
            return new List<IndividualsWithFitness<char>>
            {
                new IndividualsWithFitness<char>(new Individual<char>(genome.ToCharArray()), fitnessValue)
            }.ToImmutableList();
        }

        [Test]
        public void OnlyBestIndividualsAreKeptInArchive()
        {
            Configuration configuration = Configuration.Default()
                .WithPopulationArchiveSize(3);

            var archive = new PopulationArchive<char>(configuration, CalculateGeneticDiversity);

            archive.Update(GetIndividuals("Foo", 9));
            Assert.That(archive.Archive.Count, Is.EqualTo(1));

            archive.Update(GetIndividuals("Bar", 10));
            Assert.That(archive.Archive.Count, Is.EqualTo(2));

            archive.Update(GetIndividuals("Baz", 11));
            Assert.That(archive.Archive.Count, Is.EqualTo(3));

            archive.Update(GetIndividuals("Ban", 14));
            Assert.That(archive.Archive.Count, Is.EqualTo(3));

            archive.Update(GetIndividuals("Fu", 1));
            Assert.That(archive.Archive.Count, Is.EqualTo(3));

            Assert.That(archive.Archive.Any(x => CheckGenotype(x.GetGenotype(), "Bar")));
            Assert.That(archive.Archive.Any(x => CheckGenotype(x.GetGenotype(), "Baz")));
            Assert.That(archive.Archive.Any(x => CheckGenotype(x.GetGenotype(), "Ban")));

            Assert.That(archive.Archive.All(x => !CheckGenotype(x.GetGenotype(), "Foo")));
            Assert.That(archive.Archive.All(x => !CheckGenotype(x.GetGenotype(), "Fu")));
        }

        private static bool CheckGenotype([NotNull] IReadOnlyList<char> individual, [NotNull] string genotype)
        {
            char[] genes = genotype.ToCharArray();
            if (individual.Count != genes.Length)
            {
                return false;
            }

            for (var i = 0; i < genotype.Length; i++)
            {
                if (genes[i] != individual[i])
                {
                    return false;
                }
            }

            return true;
        }

        [Test]
        public void StatisticsGrowEachUpdate()
        {
            var archive = new PopulationArchive<char>(Configuration.Default(), CalculateGeneticDiversity);

            var counter = 1;
            while (counter < 10)
            {
                archive.Update(GetIndividuals("Foo", counter));
                Assert.That(archive.Statistics.Count, Is.EqualTo(counter));
                counter++;
            }
        }

        [Test]
        public void PreviousStatisticsAreInjectedIntoLastOnes()
        {
            var archive = new PopulationArchive<char>(Configuration.Default(), CalculateGeneticDiversity);

            archive.Update(GetIndividuals("Foo", 1));
            Assert.That(archive.Statistics.Last().BestFitness, Is.EqualTo(1));
            Assert.That(archive.Statistics.Last().ImprovementOfBest, Is.EqualTo(0));

            archive.Update(GetIndividuals("Bar", 2));
            Assert.That(archive.Statistics.Last().BestFitness, Is.EqualTo(2));
            Assert.That(archive.Statistics.Last().ImprovementOfBest, Is.EqualTo(1));

            archive.Update(GetIndividuals("Baz", 10));
            Assert.That(archive.Statistics.Last().BestFitness, Is.EqualTo(10));
            Assert.That(archive.Statistics.Last().ImprovementOfBest, Is.EqualTo(8));
        }
    }
}
