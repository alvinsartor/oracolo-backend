﻿using System;

namespace Fugro.GeneticAlgorithms.Util
{
    /// <summary>
    /// BLX-α is an algorithm to get a random value, given two boundaries and an α value, meant to extend the boundaries.
    /// <para> The new value will be a random double in the range:</para>
    ///
    /// <list type="bullet">
    /// <item>   [minValue - range * α, maxValue + range * α]    </item>
    /// </list>
    ///
    /// <para> BLX-αβ is a modification of BLX-α in which the fitness of the parents is considered to define the new range. </para>
    /// <para> The new value will be a random double in the range: </para>
    ///
    /// <list type="bullet">
    /// <item>   if (minValue ∈ bestIndividual ∧ maxValue ∈ worseIndividual)      =   [minValue - range * α, maxValue + range * β]    </item>
    /// <item>   else if (minValue ∈ worseIndividual ∧ maxValue ∈ bestIndividual) =   [minValue - range * β, maxValue + range * α]    </item>
    /// </list>
    ///
    /// </summary>
    public sealed class BLXAlpha
    {
        /// <summary>
        /// Gets a double in an interval using the BLX-α algorithm.
        /// </summary>
        /// <param name="a">The first value.</param>
        /// <param name="b">The second value.</param>
        /// <param name="alpha">The α value.</param>
        public static double GetBLXAlphaValue(double a, double b, double alpha)
        {
            return GetBLXAlphaBetaValue(a, b, alpha, alpha);
        }

        /// <summary>
        /// Gets a double in an interval using the BLX-αβ algorithm.
        /// </summary>
        /// <param name="a">The first value -Belongs to the best individual-.</param>
        /// <param name="b">The second value -Belongs to the worst individual-.</param>
        /// <param name="alpha">The α value.</param>
        /// <param name="beta">The β value.</param>
        public static double GetBLXAlphaBetaValue(double a, double b, double alpha, double beta)
        {
            double range = Math.Abs(a - b);

            double min = a < b ? a - range * alpha : b - range * beta;
            double max = a < b ? b + range * beta : a + range * alpha;

            return Random.GetRandomInRange(min, max);
        }
    }
}
