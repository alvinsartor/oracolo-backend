﻿using System.Collections.Generic;
using System.Collections.Immutable;
using AdvancedGraph.Interfaces;
using NUnit.Framework;

namespace AdvancedGraph.PathFinding.NUnit
{
    [TestFixture]
    internal sealed class PathMultigraphFixture : PathFixture
    {
        /// <inheritdoc />
        protected override Path<int> CreatePath(int initialNodeValue, params (int node, double weight)[] nodes)
        {
            var graph = new Graph<int>();
            var connectionsDictionary = new Dictionary<INode<int>, INode<int>>();

            Node<int> previous = graph.Add(initialNodeValue);
            connectionsDictionary.Add(previous, null);
            
            foreach ((var nodeValue, var weight) in nodes)
            {
                Node<int> node = graph.Add(nodeValue);
                previous.AddConnection(node, weight);

                connectionsDictionary.Add(node, previous);
                previous = node;
            }

            return new PathMultigraph<int>(new []{graph}.ToImmutableList(), connectionsDictionary, previous);
        }

        [Test]
        public void ToStringShowsTheGraphOfTheDestinationNode()
        {
            Graph<int> graphLayer0 = new Graph<int>("layer0");
            Graph<int> graphLayer1 = new Graph<int>("layer1");

            var node00 = graphLayer0.Add(0);
            var node03 = graphLayer0.Add(3);
            var node11 = graphLayer1.Add(1);
            var node12 = graphLayer1.Add(2);

            node00.AddConnection(node11);
            node11.AddConnection(node12);
            node12.AddConnection(node03);

            Path<int> path = new PathMultigraph<int>(new[] { graphLayer0, graphLayer1 }.ToImmutableList(), 
                new Dictionary<INode<int>, INode<int>>
                {
                    {node00, null},
                    {node11, node00},
                    {node12, node11},
                    {node03, node12}

                }.ToImmutableDictionary(x => x.Key, x => x.Value),
                node03);

            Assert.That(path.ToString(x => x.ToString(), showGraphName: true),
                Is.EqualTo("0 (layer0) -> 1 (layer1) -> 2 (layer1) -> 3 (layer0)"));
        }

        [Test]
        public void WhenMultipleEdgesConnectTwoNodesTheCheapestPathIsChosen()
        {
            Graph<int> graphLayer0 = new Graph<int>("layer0");
            Graph<int> graphLayer1 = new Graph<int>("layer1");

            var node00 = graphLayer0.Add(0);
            var node01 = graphLayer0.Add(1);
            var node02 = graphLayer0.Add(2);
            var node03 = graphLayer0.Add(3);

            var node10 = graphLayer1.Add(0);
            var node11 = graphLayer1.Add(1);
            var node12 = graphLayer1.Add(2);
            var node13 = graphLayer1.Add(3);

            node00.AddConnection(node01, 100);
            node01.AddConnection(node02, 10);
            node02.AddConnection(node03, 100);

            node10.AddConnection(node11, 10);
            node11.AddConnection(node12, 100);
            node12.AddConnection(node13, 10);

            Path<int> path = new PathMultigraph<int>(new[] { graphLayer0, graphLayer1 }.ToImmutableList(),
                //only nodes from the first graph are chosen, but the path will find the cheapest edges
                new Dictionary<INode<int>, INode<int>>
                {
                    {node00, null},
                    {node01, node00},
                    {node02, node01},
                    {node03, node02},

                }.ToImmutableDictionary(x => x.Key, x => x.Value),
                node03);

            Assert.That(path.ToString(x => x.ToString(), showGraphName: true),
                Is.EqualTo("0 (layer1) -> 1 (layer1) -> 2 (layer0) -> 3 (layer1)"));
            Assert.That(path.Cost, Is.EqualTo(30));
        }
    }
}
