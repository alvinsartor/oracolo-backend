﻿using System;
using System.Collections.Immutable;
using JetBrains.Annotations;
using UnityEngine;

namespace CodeAnalysis.ProjectMapping.Interfaces
{
    public interface ICsprojDescriptor : IEquatable<ICsprojDescriptor>
    {
        /// <summary>
        /// Gets the name this Csproj.
        /// </summary>
        [NotNull] string CsprojName { get; }

        /// <summary>
        /// Gets the path of this Csproj file.
        /// </summary>
        [NotNull] string FilePath { get; }

        /// <summary>
        /// Gets the path where this csproj is deployed to dll.
        /// </summary>
        [NotNull] string OutputPath { get; }

        /// <summary>
        /// Gets the files of this Csproj.
        /// </summary>
        [NotNull] ImmutableList<FileDescriptor> Files { get; }

        /// <summary>
        /// Gets the references of this Csproj.
        /// </summary>
        [NotNull] ImmutableList<ReferenceDescriptor> References { get; }

        /// <summary>
        /// Gets the SLN file that contains this Csproj.
        /// </summary>
        [NotNull] ISlnDescriptor SlnDescriptor { get; }

        /// <summary>
        /// Retrieves the content of this csproj.
        /// </summary>
        void ReloadCsprojContent();
    }
}
