﻿using System;
using JetBrains.Annotations;

namespace CodeAnalysis.Level2_Extraction_DataStructure
{
    public interface IEntity : IEquatable<IEntity>
    {
        /// <summary>
        /// Gets the entity name.
        /// </summary>
        [NotNull] string Name { get; }

        /// <summary>
        /// Gets the entity full name (namespace + name).
        /// </summary>
        [NotNull] string FullName { get; }

        /// <summary>
        /// Gets the assembly where the entity is declared.
        /// </summary>
        [NotNull] string Assembly { get; }

        /// <summary>
        /// Gets the entity status.
        /// </summary>
        EntityStatus Status { get; }

        /// <summary>
        /// Gets the entity type.
        /// </summary>
        EntityType Type { get; }
    }

    public enum EntityStatus : byte
    {
        /// <summary> The entity is referenced by another entity and has not been loaded yet. </summary>
        NotLoaded = 0,

        /// <summary> The entity has been loaded and is part of the solution. </summary>
        LoadedAndInternal = 1,

        /// <summary> The entity has been loaded and is part of an external dll. </summary>
        LoadedAndExternal = 2
    }

    public enum EntityType : byte
    {
        /// <summary> The entity type is unknown. </summary>
        Unknown = 0,

        /// <summary> The entity is an interface. </summary>
        Interface = 1,

        /// <summary> The entity is a class. </summary>
        Class = 2,

        /// <summary> The entity is a struct. </summary>
        Struct = 3
    }
}
