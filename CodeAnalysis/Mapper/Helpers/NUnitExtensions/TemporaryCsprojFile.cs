﻿using System;
using System.IO;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper.Helpers.NUnitExtensions
{
    internal sealed class TemporaryCsprojFile : IDisposable
    {
        [NotNull] private const string CsprojContent="<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                                                       "<Project ToolsVersion=\"15.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\\\"> \n" +
                                                       "<Import Project=\"..\\packages\\NUnit.3.11.0\\build\\NUnit.props\" Condition=\"Exists('..\\packages\\NUnit.3.11.0\\build\\NUnit.props')\" /> \n" +
                                                       "  <Import Project=\"$(MSBuildExtensionsPath)\\$(MSBuildToolsVersion)\\Microsoft.Common.props\" Condition=\"Exists('$(MSBuildExtensionsPath)\\$(MSBuildToolsVersion)\\Microsoft.Common.props')\" /> \n" +
                                                       "  <PropertyGroup> \n" +
                                                       "    <Configuration Condition=\" '$(Configuration)' == '' \"> Debug </Configuration> \n" +
                                                       "    <Platform Condition=\" '$(Platform)' == '' \">AnyCPU</Platform> \n" +
                                                       "    <ProjectGuid>{C4A99293-8957-4EDF-9F37-7FC41B973BFB}</ProjectGuid> \n" +
                                                       "    <OutputType>Library</OutputType> \n" +
                                                       "    <AppDesignerFolder>Properties</AppDesignerFolder> \n" +
                                                       "    <RootNamespace>AdvancedGraph</RootNamespace> \n" +
                                                       "    <AssemblyName>AdvancedGraph</AssemblyName>\n" +
                                                       "    <TargetFrameworkVersion>v4.7.2</TargetFrameworkVersion>\n" +
                                                       "    <FileAlignment>512</FileAlignment>\n" +
                                                       "    <NuGetPackageImportStamp>\n" +
                                                       "    </NuGetPackageImportStamp>\n" +
                                                       "    <TargetFrameworkProfile />\n" +
                                                       "  </PropertyGroup>\n" +
                                                       "  <PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' \">\n" +
                                                       "    <DebugSymbols> true </DebugSymbols>\n" +
                                                       "    <DebugType> full </DebugType>\n" +
                                                       "    <Optimize> false </Optimize>\n" +
                                                       "    <OutputPath>bin\\Debug\\</OutputPath>\n" +
                                                       "    <DefineConstants>DEBUG;TRACE</DefineConstants>\n" +
                                                       "    <ErrorReport>prompt</ErrorReport>\n" +
                                                       "    <WarningLevel>4</WarningLevel>\n" +
                                                       "  </PropertyGroup>\n" +
                                                       "  <PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' \">\n" +
                                                       "    <DebugType> pdbonly </DebugType>\n" +
                                                       "    <Optimize> true </Optimize>\n" +
                                                       "    <OutputPath>bin\\Release\\</OutputPath>\n" +
                                                       "    <DefineConstants>TRACE</DefineConstants>\n" +
                                                       "    <ErrorReport>prompt</ErrorReport>\n" +
                                                       "    <WarningLevel>4</WarningLevel>\n" +
                                                       "  </PropertyGroup>\n" +
                                                       "  <ItemGroup>\n" +
                                                       "    <Reference Include=\"JetBrains.Annotations, Version=2018.2.1.0, Culture=neutral, PublicKeyToken=1010a0d8d6380325, processorArchitecture=MSIL\">\n" +
                                                       "      <HintPath>..\\packages\\JetBrains.Annotations.2018.2.1\\lib\\net20\\JetBrains.Annotations.dll</HintPath>\n" +
                                                       "    </Reference>\n" +
                                                       "    <Reference Include=\"nunit.framework, Version=3.11.0.0, Culture=neutral, PublicKeyToken=2638cd05610744eb, processorArchitecture=MSIL\">\n" +
                                                       "      <HintPath>..\\packages\\NUnit.3.11.0\\lib\\net45\\nunit.framework.dll</HintPath>\n" +
                                                       "    </Reference>\n" +
                                                       "    <Reference Include=\"System\" />\n" +
                                                       "    <Reference Include=\"System.Collections.Immutable, Version=1.2.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, processorArchitecture=MSIL\">\n" +
                                                       "      <HintPath>..\\packages\\System.Collections.Immutable.1.5.0\\lib\\netstandard2.0\\System.Collections.Immutable.dll</HintPath>\n" +
                                                       "    </Reference>\n" +
                                                       "    <Reference Include=\"System.Core\" />    \n" +
                                                       "    <Reference Include=\"System.Xml\" />      \n" +
                                                       "  </ItemGroup>    \n" +
                                                       "  <ItemGroup>     \n" +
                                                       "    <Compile Include=\"Edge.cs\" /> \n" +
                                                       "    <Compile Include=\"DynamicGraph.cs\" />   \n" +
                                                       "    <Compile Include=\"Graph.cs\" />\n" +
                                                       "    <Compile Include=\"Interfaces\\IEdge.cs\" />  \n" +
                                                       "    <Compile Include=\"Interfaces\\IGraph.cs\" />\n" +
                                                       "    <Compile Include=\"Interfaces\\INode.cs\" />  \n" +
                                                       "    <Compile Include=\"Interfaces\\IDynamicGraph.cs\" />  \n" +
                                                       "    <Compile Include=\"Node.cs\" />     \n" +
                                                       "    <Compile Include=\"NodeState.cs\" /> \n" +
                                                       "    <Compile Include=\"NUnit\\DynamicGraphFixture.cs\" />  \n" +
                                                       "    <Compile Include=\"NUnit\\EdgeFixture.cs\" />\n" +
                                                       "    <Compile Include=\"NUnit\\GraphFixture.cs\" />\n" +
                                                       "    <Compile Include=\"NUnit\\GenericGraphFixture.cs\" /> \n" +
                                                       "    <Compile Include=\"NUnit\\NodeFixture.cs\" /> \n" +
                                                       "    <Compile Include=\"PathFinding\\NUnit\\PathFindingMultigraphExtensionsFixture.cs\" />\n" +
                                                       "    <Compile Include=\"PathFinding\\NUnit\\PathFindingExtensionsFixture.cs\" />\n" +
                                                       "    <Compile Include=\"PathFinding\\NUnit\\PathMultigraphFixture.cs\" />      \n" +
                                                       "    <Compile Include=\"PathFinding\\NUnit\\PathMonographFixture.cs\" /> \n" +
                                                       "    <Compile Include=\"PathFinding\\NUnit\\PathFixture.cs\" />\n" +
                                                       "    <Compile Include=\"NUnit\\ToStringExtensionsFixture.cs\" />      \n" +
                                                       "    <Compile Include=\"PathFinding\\PathMonograph.cs\" />  \n" +
                                                       "    <Compile Include=\"PathFinding\\PathMultigraph.cs\" />  \n" +
                                                       "    <Compile Include=\"PathFinding\\Path.cs\" />  \n" +
                                                       "    <Compile Include=\"PathFinding\\PathFindingMultigraphExtensions.cs\" />   \n" +
                                                       "    <Compile Include=\"PathFinding\\PathFindingMonographExtensions.cs\" />   \n" +
                                                       "    <Compile Include=\"Properties\\AssemblyInfo.cs\" />     \n" +
                                                       "    <Compile Include=\"ToStringExtensions.cs\" />\n" +
                                                       "  </ItemGroup>      \n" +
                                                       "  <ItemGroup>\n" +
                                                       "    <None Include=\"packages.config\" /> \n" +
                                                       "  </ItemGroup> \n" +
                                                       "  <ItemGroup>\n" +
                                                       "    <ProjectReference Include=\"..\\Extensions\\Extensions.csproj\">\n" +
                                                       "      <Project>{D1290E01-C5F4-4298-81F4-EB27CF76AE62}</Project>  \n" +
                                                       "      <Name>Extensions</Name>  \n" +
                                                       "    </ProjectReference> \n" +
                                                       "  </ItemGroup> \n" +
                                                       "  <Import Project=\"$(MSBuildToolsPath)\\Microsoft.CSharp.targets\" />\n" +
                                                       "  <Target Name=\"EnsureNuGetPackageBuildImports\" BeforeTargets=\"PrepareForBuild\">    \n" +
                                                       "    <PropertyGroup>    \n" +
                                                       "      <ErrorText>This project references NuGet package(s) that are missing on this computer.Use NuGet Package Restore to download them.For more information, see http://go.microsoft.com/fwlink/?LinkID=322105. The missing file is {0}.</ErrorText>    \n" +
                                                       "    </PropertyGroup>   \n" +
                                                       "    <Error Condition=\"!Exists('..\\packages\\NUnit.3.11.0\\build\\NUnit.props')\" Text= \"$([System.String]::Format('$(ErrorText)', '..\\packages\\NUnit.3.11.0\\build\\NUnit.props'))\" />\n" +
                                                       "  </Target>\n" +
                                                       "</Project> ";

        [NotNull] private readonly string m_TemporaryFilePath;

        public TemporaryCsprojFile()
        {
            m_TemporaryFilePath=Path.Combine(Path.GetTempPath(), "TemporaryCsproj.csproj");
            File.WriteAllText(m_TemporaryFilePath, CsprojContent);
        }

        [NotNull]
        public string FilePath => m_TemporaryFilePath;

        /// <inheritdoc />
        public void Dispose()
        {
            File.Delete(m_TemporaryFilePath);
        }
    }
}