﻿using System;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph
{
    public static class ToStringExtensions
    {
        // - - NODES - -

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="valueToString">Function to obtain the string representation from the value of the node.</param>
        [NotNull]
        public static string ToString<T>([NotNull] this INode<T> node, [NotNull] Func<T, string> valueToString)
        {
            node.AssertNotNull(nameof(node));
            return $"Node ID: {node.Id.ToString()}, Value: {valueToString(node.Value)}";
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="valueToString">Function to obtain the string representation from the value of the node.</param>
        [NotNull]
        public static string ToStringValues<T>([NotNull] this INode<T> node, [NotNull] Func<T, string> valueToString)
        {
            node.AssertNotNull(nameof(node));

            var edges = "";
            foreach (var edge in node.Edges.Values)
            {
                edges += $" -> {valueToString(edge.DestinationNode.Value)} [w:{edge.Weight}] \n";
            }

            return $"Node: {valueToString(node.Value)} \n{edges}";
        }

        // - - EDGES - -

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <param name="edge">The edge.</param>
        /// <param name="toString">Function to obtain the string representation from the value of the node.</param>
        [NotNull]
        public static string ToStringValues<T>([NotNull] this IEdge<T> edge, [NotNull] Func<T, string> toString)
        {
            edge.AssertNotNull(nameof(edge));
            return $"'{toString(edge.StartNode.Value)}' -> '{toString(edge.DestinationNode.Value)}', [w:{edge.Weight}]";
        }
    
    }
}