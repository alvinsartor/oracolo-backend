﻿using Fugro.NUnitExtensions;
using Fugro.NUnitExtensions.Behavior;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Components.NUnit
{
    [TestFixture]
    internal sealed class ConfigurationFixture : BaseContractsFixture<Configuration>
    {
        /// <inheritdoc />
        protected override Configuration AllocatePopulated()
        {
            return Configuration.Default()
                .WithAlphaCrossover(0.12)
                .WithBetaCrossover(0.06)
                .WithCalculateGeneticDiversity(true)
                .WithPopulationSize(42);
        }

        /// <inheritdoc />
        protected override Configuration AllocateDefault()
        {
            return Configuration.Default();
        }

        [Test]
        public void ConfigurationIsInstantiatedCorrectly()
        {
            const double AlphaCrossover = 0.15;
            const double BetaCrossover = 0.08;

            const double AlphaMutation = 0.15;
            const double MutationProbability = 0.03;

            const int TournamentSize = 5;
            const int GeneticPressureForRanking = 2;
            const bool IsMaximizationProblem = true;

            const int PopulationSize = 100;
            const int PopulationArchiveSize = 100;
            const double IndividualsToReplace = 0.9;

            const double MinimumFitnessRequired = 0.97;
            const int MaximumNumberOfGenerationsWithoutImprovements = int.MaxValue;
            const int MaximumNumberOfGenerations = int.MaxValue;

            const bool CalculateGeneticDiversity = false;

            var configuration = new Configuration(
                AlphaCrossover,
                BetaCrossover,
                AlphaMutation,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRanking,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessRequired,
                MaximumNumberOfGenerationsWithoutImprovements,
                MaximumNumberOfGenerations,
                CalculateGeneticDiversity);

            configuration.AlphaCrossoverValue.ShouldBe(AlphaCrossover);
            configuration.BetaCrossoverValue.ShouldBe(BetaCrossover);

            configuration.AlphaMutationValue.ShouldBe(AlphaMutation);
            configuration.MutationProbability.ShouldBe(MutationProbability);

            configuration.TournamentSize.ShouldBe(TournamentSize);
            configuration.GeneticPressureForRankingSelection.ShouldBe(GeneticPressureForRanking);
            configuration.IsMaximizationProblem.ShouldBe(IsMaximizationProblem);

            configuration.PopulationSize.ShouldBe(PopulationSize);
            configuration.PopulationArchiveSize.ShouldBe(PopulationArchiveSize);
            configuration.IndividualsToReplace.ShouldBe(IndividualsToReplace);

            configuration.MinimumFitnessToStopEvolutionaryProcess.ShouldBe(MinimumFitnessRequired);
            configuration.MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess.ShouldBe(MaximumNumberOfGenerationsWithoutImprovements);
            configuration.MaximumGenerationToStopEvolutionaryProcess.ShouldBe(MaximumNumberOfGenerations);

            configuration.CalculateGeneticDiversity.ShouldBe(CalculateGeneticDiversity);
        }
    }
}
