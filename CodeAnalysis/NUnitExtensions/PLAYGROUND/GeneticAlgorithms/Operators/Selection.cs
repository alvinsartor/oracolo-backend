﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Util;
using JetBrains.Annotations;
using Random = Fugro.GeneticAlgorithms.Util.Random;

namespace Fugro.GeneticAlgorithms.Operators
{
    /// <summary>
    /// The selection algorithms are used to select the individuals used to create the next generation.
    ///
    /// <para>These are standard selection algorithms.
    ///
    /// Even though it is adviced to use these standard algorithms,
    /// the user can always write a personalised algorithm that fits better with the problem.
    /// </para>
    /// </summary>
    public static class Selection
    {
        /// <summary>
        /// Performs a roulette selection over the array of individuals
        /// </summary>
        /// <typeparam name="T">Type of the individuals</typeparam>
        /// <param name="individuals">The individuals.</param>
        /// <param name="fitnesses">The fitnesses.</param>
        /// <param name="isMaximizationProblem">True if the highest fitness is the best, false otherwise</param>
        /// <param name="numberOfIndividuals">The number of individuals we want to select.</param>
        /// <remarks>
        /// A ROULETTE selection assigns a probability of being selected to each individual relative to its fitness.
        /// This means that fitter individuals will have more probabilities of being chosen, while not fit individual will have fewer.
        ///
        /// The roulette selection is designed to obtain a fast convergence, the price is, though, genetic diversity.
        /// <para>Axiomatic complexity: O(n) </para>
        /// </remarks>
        [NotNull]
        public static Queue<T> RouletteSelection<T>(
            [NotNull] ImmutableList<T> individuals,
            [NotNull] double[] fitnesses,
            bool isMaximizationProblem,
            int numberOfIndividuals)
        {
            ArgumentHelper.AssertNotNull(fitnesses, nameof(fitnesses));
            ArgumentHelper.AssertNotNull(individuals, nameof(individuals));

            if (!isMaximizationProblem)
            {
                throw new InvalidOperationException("The roulette algorithm can't be used for minimization problems");
            }
            if (fitnesses.Any(x => x < 0))
            {
                throw new InvalidOperationException("The roulette algorithm can't be used with negative fitnesses");
            }

            double populationFitness = fitnesses.Sum();
            IEnumerable<double> ratios = fitnesses.Select(x => x / populationFitness);
            double[] accumulation = ratios.CumulativeSum().ToArray();

            var selectedIndividuals = new Queue<T>();

            while (selectedIndividuals.Count < numberOfIndividuals)
            {
                var index = 0;
                double power = Random.GetRandomDouble();
                while (power > accumulation[index])
                {
                    index++;
                }

                selectedIndividuals.Enqueue(individuals[index]);
            }

            return selectedIndividuals;
        }


        /// <summary>
        /// Performs a ranking selection over the array of individuals
        /// </summary>
        /// <typeparam name="T">Type of the individuals</typeparam>
        /// <param name="individuals">The individuals.</param>
        /// <param name="fitnesses">The fitnesses.</param>
        /// <param name="isMaximizationProblem">True if the highest fitness is the best, false otherwise</param>
        /// <param name="numberOfIndividuals">The number of individuals we want to select.</param>
        /// <param name="selectivePressure">The importance we give to an individual using his rank. Keep in range: [1,5]. Note: it grows exponentially!</param>
        /// <remarks>
        /// A RANKING selection orders the individuals by fitness and selects them using their ranks.
        ///
        /// The ranking selection is useful when there is a huge difference in fitness of similar individuals. The roulette selection would penalize this difference,
        /// while the ranking selection would see the two individual as really close to each other.
        ///
        /// The genetic diversity price of this algorithm is HIGH.
        /// <para> Axiomatic complexity: O(n * Log(n)) </para>
        /// </remarks>
        [NotNull]
        public static Queue<T> RankingSelection<T>(
            [NotNull] ImmutableList<T> individuals,
            [NotNull] double[] fitnesses,
            bool isMaximizationProblem,
            int numberOfIndividuals,
            int selectivePressure)
        {
            ArgumentHelper.AssertNotNull(fitnesses, nameof(fitnesses));
            ArgumentHelper.AssertNotNull(individuals, nameof(individuals));

            T[] individualsCopy = individuals.ToArray();
            double[] fitnessesCopy = fitnesses.ToArray();
            Array.Sort(fitnessesCopy, individualsCopy);

            if (!isMaximizationProblem)
            {
                Array.Reverse(individualsCopy);
            }

            double[] weights = individualsCopy.Select((v, i) => Math.Pow(i + 1, selectivePressure)).ToArray();

            return RouletteSelection(individualsCopy.ToImmutableList(), weights, true, numberOfIndividuals);
        }

        /// <summary>
        /// Performs a tournament selection over the array of individuals
        /// </summary>
        /// <typeparam name="T">Type of the individuals</typeparam>
        /// <param name="individuals">The individuals.</param>
        /// <param name="fitnesses">The fitnesses.</param>
        /// <param name="isMaximizationProblem">True if the highest fitness is the best, false otherwise</param>
        /// <param name="numberOfIndividuals">The number of individuals we want to select.</param>
        /// <param name="tournamentSize">The size of the tournament. Inversely proportional to genetic diversity.</param>
        /// <remarks>
        /// A TOURNAMENT selection picks N random individual from the population and selects the fittest.
        ///
        /// This selection algorithm is pretty simple but effective: there is a fairly high chance that also the less performing part of the population
        /// will participate to the creation of the next generation and this means that the genetic diversity is kept high.
        ///
        /// The genetic diversity price of this algorithm is HIGH.
        /// <para>Axiomatic complexity: O(tournamentSize * n) </para>
        /// </remarks>
        [NotNull]
        public static Queue<T> TournamentSelection<T>(
            [NotNull] ImmutableList<T> individuals,
            [NotNull] double[] fitnesses,
            bool isMaximizationProblem,
            int numberOfIndividuals,
            int tournamentSize)
        {
            ArgumentHelper.AssertNotNull(fitnesses, nameof(fitnesses));
            ArgumentHelper.AssertNotNull(individuals, nameof(individuals));

            var selectedIndividuals = new Queue<T>();
            var tournamentParticipants = new int[tournamentSize];

            while (selectedIndividuals.Count < numberOfIndividuals)
            {
                for (var i = 0; i < tournamentSize; i++)
                {
                    tournamentParticipants[i] = Random.GetRandomInRange(0, individuals.Count);
                }

                int winner = isMaximizationProblem
                    ? tournamentParticipants.MaxBy(x => fitnesses[x])
                    : tournamentParticipants.MinBy(x => fitnesses[x]);

                selectedIndividuals.Enqueue(individuals[winner]);
            }

            return selectedIndividuals;
        }
    }
}