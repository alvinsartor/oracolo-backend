﻿using System;
using System.Collections.Generic;
using System.IO;
using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.ProjectMapping
{
    public sealed class FileDescriptor
    {
        [NotNull] public static readonly HashSet<string> SupportedFileTypes = new HashSet<string>
        {
            ".cs"
        };

        /// <summary>
        /// Gets the file path.
        /// </summary>
        [NotNull] public string FilePath { get; }

        /// <summary>
        /// Gets the name of the file (the last part of the path).
        /// </summary>
        [NotNull] public string FileName { get; }

        /// <summary>
        /// Gets the csproj this file belongs to.
        /// </summary>
        [NotNull] public ICsprojDescriptor Csproj { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDescriptor"/> class.
        /// </summary>
        /// <param name="filePath">The path of the file.</param>
        /// <param name="csproj">The csproj descriptor.</param>
        internal FileDescriptor([NotNull] string filePath, [NotNull] ICsprojDescriptor csproj)
        {
            CheckFilePath(filePath);

            FilePath = filePath;
            FileName = Path.GetFileName(filePath);

            Csproj = csproj.NotNull(nameof(csproj));
        }

        [ContractAnnotation("filePath : Null => halt")]
        private static void CheckFilePath([NotNull] string filePath)
        {
            filePath.AssertNotNull(nameof(filePath));

            if (!filePath.Contains(Path.DirectorySeparatorChar.ToString()))
            {
                throw  new ArgumentException($"The given path seems incorrect: {filePath}");
            }

            if (!Path.HasExtension(filePath) || !SupportedFileTypes.Contains(Path.GetExtension(filePath)))
            {
                throw new ArgumentException($"File extension inexistent or not supported: {filePath}");
            }
        }

        /// <summary>
        /// Gets the file name without the extension.
        /// </summary>
        [NotNull]
        public string FileNameWithoutExtension => FileName.Remove(FileName.LastIndexOf('.'));
    }
}
