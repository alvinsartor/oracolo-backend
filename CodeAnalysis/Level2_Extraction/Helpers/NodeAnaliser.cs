﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Extensions;
using JetBrains.Annotations;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeAnalysis.Level2_Extraction.Helpers
{
    internal static class NodeAnaliser
    {
        /// <summary>
        /// Gets the nodes definiton. Used only for nodes found in the inheritances.
        /// This function considers only the first level in generic nodes.
        /// </summary>
        /// <param name="syntaxNode">The syntax node.</param>
        /// <param name="model">The model.</param>
        /// <exception cref="ArgumentException">The specified node is not supported.</exception>
        [NotNull]
        public static string GetNodesDefinitonForInheritances([NotNull] SyntaxNode syntaxNode, [NotNull] SemanticModel model)
        {
            switch (syntaxNode)
            {
                case IdentifierNameSyntax identifierName:
                    return model.GetSymbolInfo(identifierName).Symbol.OriginalDefinition.ToString();
                case GenericNameSyntax genericName:
                    return model.GetTypeInfo(genericName).Type.OriginalDefinition.ToString();
                default:
                    throw new ArgumentException("The specified node is not supported.");
            }
        }

        [NotNull]
        public static ImmutableList<string> GetNodeOriginalDefinitions([NotNull] SyntaxNode syntaxNode, [NotNull] SemanticModel model)
        {
            switch (syntaxNode)
            {
                case IdentifierNameSyntax identifierName:
                    return SimpleNameExtraction(identifierName, model).Enlist();
                case GenericNameSyntax genericName:
                    return GetTypesOfGenericNode(genericName, model);
                case TupleTypeSyntax tupleName:
                    return GetTypesFromTuples(tupleName, model);
                case ArrayTypeSyntax arrayName:
                    return GetTypeFromArray(arrayName, model);
                default:
                    // this operation may throw exceptions.
                    return SimpleNameExtraction(syntaxNode, model).Enlist();
            }
        }

        private static ImmutableList<string> GetTypeFromArray(ArrayTypeSyntax arrayName, SemanticModel model)
        {
            return arrayName.ChildNodes().First()
                .Apply(node => GetNodeOriginalDefinitions(node, model));
        }

        [NotNull]
        private static ImmutableList<string> GetTypesFromTuples([NotNull] SyntaxNode tupleName, [NotNull] SemanticModel model)
        {
            return tupleName.ChildNodes().OfType<TupleElementSyntax>()
                .SelectMany(tupleElement => GetNodeOriginalDefinitions(tupleElement.ChildNodes().First(), model))
                .ToImmutableList();
        }

        [NotNull]
        private static string SimpleNameExtraction([NotNull] SyntaxNode syntaxNode, [NotNull] SemanticModel model)
        {
            return model.GetSymbolInfo(syntaxNode).Symbol.OriginalDefinition.ToString();
        }

        [NotNull]
        private static ImmutableList<string> GetTypesOfGenericNode([NotNull] SyntaxNode node, [NotNull] SemanticModel model)
        {
            HashSet<string> result = new HashSet<string>();
            string genericNodeType = model.GetTypeInfo(node).Type.OriginalDefinition.ToString();
            result.Add(genericNodeType);

            SeparatedSyntaxList<TypeSyntax> arguments = node.ChildNodes().OfType<TypeArgumentListSyntax>().Single().Arguments;
            foreach (var argument in arguments)
            {
                IEnumerable<string> types = argument is GenericNameSyntax genericName
                    ? GetTypesOfGenericNode(genericName, model)
                    : SimpleNameExtraction(argument, model).Enumerate();

                types.ForEach(x => result.Add(x));
            }

            return result.ToImmutableList();
        }

        [NotNull]
        public static ImmutableList<string> GetInnerTypeOfGenericNode([NotNull] GenericNameSyntax node, [NotNull] SemanticModel model)
        {
            HashSet<string> result = new HashSet<string>();
            SeparatedSyntaxList<TypeSyntax> arguments = node.ChildNodes().OfType<TypeArgumentListSyntax>().Single().Arguments;
            foreach (var argument in arguments)
            {
                IEnumerable<string> types = argument is GenericNameSyntax genericName
                    ? GetTypesOfGenericNode(genericName, model)
                    : SimpleNameExtraction(argument, model).Enumerate();

                types.ForEach(x => result.Add(x));
            }

            return result.ToImmutableList();
        }

        [NotNull]
        public static IEnumerable<string> BruteForceTypeSearch([NotNull] SyntaxNode syntaxNode, [NotNull] SemanticModel model)
        {
            IEnumerable<SyntaxNode> descendants = syntaxNode.DescendantNodesAndSelf();
            var tasks = descendants.Select(node => Task.Run(() => GetTypeIfPossible(node, model)));

            return Task.WhenAll(tasks).Result.Distinct().Where(IsNotBruteForceGarbage);

            bool IsNotBruteForceGarbage(string s) => s != null && s != "?" & s != "void";
        }

        [CanBeNull]
        private static string GetTypeIfPossible([NotNull] SyntaxNode node, [NotNull] SemanticModel model)
        {
            return model.GetTypeInfo(node).Type?.OriginalDefinition.ToString().Apply(RemoveBrackets);
        }

        [NotNull]
        private static string RemoveBrackets([NotNull] string variableType)
        {
            return !variableType.EndsWith("[]")
                ? variableType
                : variableType.Substring(0, variableType.Length - 2);
        }
    }
}
