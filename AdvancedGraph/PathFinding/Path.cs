﻿using System;
using System.Collections.Immutable;
using System.Linq;
using AdvancedGraph.Interfaces;
using JetBrains.Annotations;

namespace AdvancedGraph.PathFinding
{
    public class Path<T>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Path{T}"/> class from being created.
        /// </summary>
        protected Path()
        {
            Id = Guid.NewGuid();
            Edges = ImmutableList<IEdge<T>>.Empty;
        }

        /// <summary>
        /// Gets the path identifier.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Gets an empty path.
        /// </summary>
        [NotNull]
        public static Path<T> Empty => new Path<T>();

        /// <summary>
        /// Gets a value indicating whether this path is empty.
        /// </summary>
        public bool IsEmpty => Edges.Count == 0;

        /// <summary>
        /// Gets the edges of the path.
        /// </summary>
        [NotNull] public ImmutableList<IEdge<T>> Edges { get; protected set; }

        /// <summary>
        /// Gets the nodes of the path.
        /// </summary>
        [NotNull] public ImmutableList<INode<T>> Nodes => Edges.Count > 0
            ? Edges.Select(x => x.StartNode).ToImmutableList().Add(Edges.Last().DestinationNode)
            : ImmutableList<INode<T>>.Empty;

        /// <summary>
        /// Gets the total cost of the path.
        /// </summary>
        public double Cost => Edges.Count > 0
            ? Edges.Select(x => x.Weight).Sum()
            : 0;

        /// <inheritdoc />
        public override string ToString()
        {
            if (IsEmpty)
            {
                return "The path is empty.";
            }

            string result = $"{Edges[0].StartNode}";
            foreach (var edge in Edges)
            {
                result += $" -> {edge.DestinationNode}";
            }

            return result;
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <param name="toString">Function to obtain the string representation from the value of the node.</param>
        /// <param name="showWeights">>Determines wheater the connection weights are printed.</param>
        /// <param name="showGraphName">Determines wheater the graph name is printed for each node (useful when using multigraphs).</param>
        [NotNull]
        public string ToString([NotNull] Func<T, string> toString, bool showWeights = false, bool showGraphName = false)
        {
            string GraphName(INode<T> node) => showGraphName ? $" ({node.Graph.Name})" : "";
            string EdgeWeight(IEdge<T> edge) => showWeights ? $"({edge.Weight})" : "";

            if (IsEmpty)
            {
                return "The path is empty.";
            }

            string result = $"{toString(Edges[0].StartNode.Value)}{GraphName(Edges[0].StartNode)}";
            foreach (var edge in Edges)
            {
                result += $" ->{EdgeWeight(edge)} {toString(edge.DestinationNode.Value)}{GraphName(edge.DestinationNode)}";
            }

            return result;
        }
    }
}