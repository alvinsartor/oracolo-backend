﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeAnalysis.Level2_Extraction;
using CodeAnalysis.Level2_Extraction.Helpers;
using CodeAnalysis.Level2_Extraction_DataStructure;
using Extensions;
using JetBrains.Annotations;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeAnalysis.Level3_Refinement
{
    internal sealed class RefinementManager : IDisposable
    {
        [NotNull] public readonly ExtractionManager m_ExtractionManager;

        [NotNull] private Dictionary<string, IEntity> m_KnowEntities;

        [NotNull] private Dictionary<string, LoadedEntity> m_LoadedEntities;
        [NotNull] private Dictionary<string, NotLoadedEntity> m_NotLoadedEntities;
        [NotNull] private Dictionary<string, ExternalEntity> m_ExternalEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="RefinementManager"/> class.
        /// </summary>
        public RefinementManager()
        {
            m_ExtractionManager = new ExtractionManager();

            m_KnowEntities = new Dictionary<string, IEntity>();
            m_LoadedEntities = new Dictionary<string, LoadedEntity>();
            m_NotLoadedEntities = new Dictionary<string, NotLoadedEntity>();
            m_ExternalEntities = new Dictionary<string, ExternalEntity>();
        }

        /// <summary>
        /// Gather and returns the entities contained in the directory.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        [NotNull]
        public ImmutableList<EntityInfo> RefineDirectory([NotNull] string directoryPath)
        {
            //directoryPath.AssertNotNull(nameof(directoryPath));
            //ImmutableDictionary<string, SyntaxTree> extractedData = m_ExtractionManager.ExtractDirectory(directoryPath);
            //return ProcessFilesContents(extractedData);
            return ImmutableList<EntityInfo>.Empty;
        }

        /// <summary>
        ///  Gather and returns the entities contained in the directory.
        /// </summary>
        /// <param name="filePaths">The file paths.</param>
        [NotNull]
        public ImmutableList<EntityInfo> RefineFiles([NotNull] ImmutableList<string> filePaths)
        {
            //filePaths.AssertNotNull(nameof(filePaths));
            //ImmutableDictionary<string, SyntaxTree> extractedData = m_ExtractionManager.Extract(filePaths);
            //return ProcessFilesContents(extractedData);
            return ImmutableList<EntityInfo>.Empty;
        }

        [NotNull]
        private ImmutableList<EntityInfo> ProcessFilesContents([NotNull] ImmutableDictionary<string, SyntaxTree> trees)
        {
            ////first we exclude the already loaded ones
            //Dictionary<string, SyntaxTree> notLoadedEntities = trees
            //    .Where(x => !m_LoadedEntities.ContainsKey(x.Key))
            //    .ToDictionary(x => x.Key, x => x.Value);

            ////then we identify the notLoadedEntities (these will be loaded, so the collections must be updated)
            //ImmutableList<string> toBeLoaded = notLoadedEntities
            //    .Where(x => m_NotLoadedEntities.ContainsKey(x.Key))
            //    .Select(x => x.Key).ToImmutableList();
            //toBeLoaded.ForEach(entity => m_NotLoadedEntities.Remove(entity));

            ////we analise all entities and get in return lists with dependencies and inheritances
            //ImmutableList<(LoadedEntity entity,
            //    ImmutableList<(string assembly, string fullName, EntityType type)> inheritances,
            //    ImmutableList<(string assembly, string fullName, EntityType type)> dependencies)> entitiesToResolve = RefineEntities(
            //    notLoadedEntities, e => m_ExtractionManager.GetSemanticModel(e));

            ////updating the collections: step1 - add newly loaded entities
            //entitiesToResolve.ForEach(x =>
            //{
            //    m_KnowEntities[x.entity.FullName] = x.entity;
            //    m_LoadedEntities[x.entity.FullName] = x.entity;
            //});

            ////step2 - analyse dependencies and inheritances to find new entities
            //foreach (var (_, inheritances, dependencies) in entitiesToResolve)
            //{
            //    foreach ((var assembly, var fullName, var type) in inheritances.Concat(dependencies))
            //    {
            //        if (!m_KnowEntities.ContainsKey(fullName))
            //        {
            //            if (m_ExtractionManager.IsAssemblyPartOfSolution(assembly))
            //            {
            //                var e = new NotLoadedEntity(fullName, assembly, type);
            //                m_KnowEntities.Add(fullName, e);
            //                m_NotLoadedEntities.Add(fullName, e);
            //            }
            //            else
            //            {
            //                var e = new ExternalEntity(fullName, assembly, type);
            //                m_KnowEntities.Add(fullName, e);
            //                m_ExternalEntities.Add(fullName, e);
            //            }
            //        }
            //        else if (!m_LoadedEntities.ContainsKey(fullName)
            //                 && !m_NotLoadedEntities.ContainsKey(fullName)
            //                 && m_ExternalEntities.ContainsKey(fullName))
            //        {
            //            throw new InvalidOperationException($"Incongruence with the data structures: {fullName} is known, but impossible to find");
            //        }
            //    }
            //}

            ////now that the entities have been inserted it is possible to build the List<EntityInfo>
            //ImmutableList<EntityInfo> result = entitiesToResolve.Select(e =>
            //        new EntityInfo(e.entity,
            //            e.inheritances.Select(r => m_KnowEntities[r.fullName]).ToImmutableList(),
            //            e.dependencies.Select(r => m_KnowEntities[r.fullName]).ToImmutableList()))
            //    .ToImmutableList();

            //return result;

            return null;
        }

        [NotNull]
        private ImmutableList<EntityInfo> LoadEntities([NotNull] ImmutableList<NotLoadedEntity> entities)
        {
            throw new NotImplementedException();
        }

        [NotNull]
        private static ImmutableList<(LoadedEntity entity,
            ImmutableList<(string assembly, string fullName, EntityType type)> inheritances,
            ImmutableList<(string assembly, string fullName, EntityType type)> dependencies)> 
            RefineEntities(
            [NotNull] IEnumerable<KeyValuePair<string, SyntaxTree>> trees,
            [NotNull] Func<string, SemanticModel> getSemanticModel)
        {
            trees.AssertNotNull(nameof(trees));
            getSemanticModel.AssertNotNull(nameof(getSemanticModel));

            return trees.SelectMany(tree => RefineSingleFile(tree.Key, tree.Value, getSemanticModel(tree.Key))).ToImmutableList();
        }

        [NotNull]
        private static ImmutableList<(LoadedEntity entity,
                ImmutableList<(string assembly, string fullName, EntityType type)> inheritances,
                ImmutableList<(string assembly, string fullName, EntityType type)> dependencies)>
            RefineSingleFile(
            [NotNull] string treeFilePath,
            [NotNull] SyntaxTree syntaxTree,
            [NotNull] SemanticModel model)
        {
            IEnumerable<SyntaxNode> entitiesInFile = ExtractEntitiesFromTree(syntaxTree);
            return entitiesInFile.Select(entity => RefineSingleEntity(treeFilePath, entity, model)).ToImmutableList();
        }

        [NotNull]
        private static IEnumerable<SyntaxNode> ExtractEntitiesFromTree([NotNull] SyntaxTree syntaxTree)
        {
            return syntaxTree.GetRoot().DescendantNodes().OfType<TypeDeclarationSyntax>();
        }

        [NotNull]
        private static (LoadedEntity entity,
            ImmutableList<(string assembly, string fullName, EntityType type)> inheritances,
            ImmutableList<(string assembly, string fullName, EntityType type)> dependencies) 
            RefineSingleEntity(
            [NotNull] string treeFilePath,
            [NotNull] SyntaxNode typeDeclarationNode,
            [NotNull] SemanticModel model)
        {
            //var loadedEntity = new LoadedEntity(
            //    SyntaxTreeAnaliser.ExtractSyntaxNodeFullName(typeDeclarationNode, model),
            //    SyntaxTreeAnaliser.ExtractAssembly(typeDeclarationNode, model),
            //    treeFilePath,
            //    SyntaxTreeAnaliser.ExtractEntityType(typeDeclarationNode, model));

            return (null, 
                SyntaxTreeAnaliser.GetInheritances(typeDeclarationNode, model),
                SyntaxTreeAnaliser.GetDependencies(typeDeclarationNode, model));            
        }

        /// <inheritdoc />
        public void Dispose()


        {
            m_ExtractionManager.Dispose();
        }
    }
}