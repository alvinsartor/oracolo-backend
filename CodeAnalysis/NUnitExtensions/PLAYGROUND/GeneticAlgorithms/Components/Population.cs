﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components
{
    /// <summary>
    /// A population is a collection of individuals.
    /// <para>The Population class is used to evolve individuals, using the parameters passed in the constructor
    /// and the genetic operators defined by the user.</para>
    /// </summary>
    /// <typeparam name="T">Type of the individuals</typeparam>
    public abstract class Population<T>
    {
        private readonly Configuration m_Configuration;
        private ImmutableList<Individual<T>> m_Individuals;

        private bool m_HasFinished;
        private ImmutableList<T> m_BestIndividualsGenome;
        private double m_BestFitness;
        private int m_GenerationNumber = 1;
        private int m_GenerationsWithoutImprovements;

        [NotNull] private readonly PopulationArchive<T> m_Archive;

        /// <summary>
        /// Gets a value indicating whether this instance has finished.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has finished; otherwise, <c>false</c>.
        /// </value>
        public bool HasFinished => m_HasFinished;

        /// <summary>
        /// Gets the best individual found until now.
        /// </summary>
        /// <exception cref="InvalidOperationException">Evolution hasn't been performed yet</exception>
        [NotNull]
        public ImmutableList<T> BestIndividual =>
            m_BestIndividualsGenome ?? throw new InvalidOperationException("Evolution hasn't been performed yet.");

        /// <summary>
        /// The best fitness found until now.
        /// </summary>
        public double BestFitness => m_BestFitness;

        /// <summary>
        /// The generation number. This value increases every time the UpdateWithResults function is called
        /// </summary>
        public int GenerationNumber => m_GenerationNumber;

        /// <summary>
        /// The configuration used during the evolutionary process.
        /// </summary>
        [NotNull]
        public Configuration Configuration => m_Configuration;

        /// <summary>
        /// Gets the genomes in the poulation archive.
        /// </summary>
        [NotNull] public ImmutableList<ImmutableList<T>> GetGenomesInArchive => m_Archive.Archive.Select(x => x.GetGenotype()).ToImmutableList();

        /// <summary>
        /// Gets the statistics for each generation that has been updated until now.
        /// </summary>
        [NotNull] public ImmutableList<GenerationStatistics<T>> GetStatistics => m_Archive.Statistics;

        /// <summary>
        /// Initializes a new instance of the <see cref="Population{T}"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        protected Population([CanBeNull] Configuration configuration = null)
        {
            m_Configuration = configuration ?? Configuration.Default();
            m_BestFitness = m_Configuration.IsMaximizationProblem ? double.MinValue : double.MaxValue;

            m_Archive = new PopulationArchive<T>(m_Configuration, CalculateGeneticDiversity);
        }

        /// <summary>
        /// Gets the individuals of the current generation.
        /// </summary>
        [NotNull]
        public List<ImmutableList<T>> GetIndividuals()
        {
            if (m_Individuals == null)
            {
                m_Individuals = Initialization();
            }

            return m_Individuals.Select(individual => individual.Genome).ToList();
        }

        /// <summary>
        /// Updates the individual list using genetic operators.
        /// </summary>
        /// <param name="fitnesses">The fitnesses of the last experiment.</param>
        public virtual void UpdateWithResults([NotNull] double[] fitnesses)
        {
            ArgumentHelper.AssertNotNull(fitnesses, nameof(fitnesses));

            if (m_Individuals == null)
            {
                throw new InvalidOperationException("Individuals haven't been checked. Get the individuals before updating with results.");
            }

            ImmutableList<IndividualsWithFitness<T>> individualsWithFitnesses =
                m_Individuals.Select((v, i) => new IndividualsWithFitness<T>(v, fitnesses[i])).ToImmutableList();

            UpdateInternalVariables(individualsWithFitnesses);

            if (m_HasFinished)
            {
                return;
            }

            Queue<IndividualsWithFitness<T>> parents = Selection(individualsWithFitnesses, fitnesses);
            ImmutableList<Individual<T>> newPopulation = GetNewPopulation(parents);
            m_Individuals = Replacement(m_Individuals, newPopulation);

            m_GenerationNumber++;
        }

        private void UpdateInternalVariables([NotNull] ImmutableList<IndividualsWithFitness<T>> individualsWithFitnesses)
        {
            m_Archive.Update(individualsWithFitnesses);
            GenerationStatistics<T> statistics = m_Archive.Statistics.Last();

            if (statistics.HasFitnessImproved() || m_BestIndividualsGenome == null)
            {
                m_BestFitness = statistics.BestFitness;
                m_BestIndividualsGenome = statistics.BestIndividual.GetGenotype();
                m_GenerationsWithoutImprovements = 0;
            }
            else
            {
                m_GenerationsWithoutImprovements++;
            }

            m_HasFinished = CheckTerminationConditions();
        }

        private bool CheckTerminationConditions()
        {
            return HasReachedFitnessObjective(m_BestFitness) ||
                   m_GenerationNumber >= m_Configuration.MaximumGenerationToStopEvolutionaryProcess ||
                   m_GenerationsWithoutImprovements >= m_Configuration.MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess;
        }

        private bool HasReachedFitnessObjective(double newFitness)
        {
            return m_Configuration.IsMaximizationProblem
                ? newFitness >= Configuration.MinimumFitnessToStopEvolutionaryProcess
                : newFitness <= Configuration.MinimumFitnessToStopEvolutionaryProcess;
        }

        [NotNull, ItemNotNull]
        private ImmutableList<Individual<T>> GetNewPopulation([NotNull] Queue<IndividualsWithFitness<T>> parents)
        {
            var newPopulation = new List<Individual<T>>();
            while (parents.Count > 0)
            {
                IndividualsWithFitness<T> parent1 = parents.Dequeue();
                IndividualsWithFitness<T> parent2 = parents.Dequeue();

                ImmutableList<T> bestGenotype = parent1 > parent2 ? parent1.GetGenotype() : parent2.GetGenotype();
                ImmutableList<T> worseGenotype = parent1 > parent2 ? parent2.GetGenotype() : parent1.GetGenotype();

                ImmutableList<T> newGenotype = Crossover(bestGenotype, worseGenotype);
                ImmutableList<T> mutatedGenotype = Mutation(newGenotype);

                var newBorn = new Individual<T>(mutatedGenotype);

                newPopulation.Add(newBorn);
            }

            return newPopulation.ToImmutableList();
        }

        /// <summary>
        /// The algorithm used to initialize the population.
        /// </summary>
        /// <returns>A list with the new individuals, generally randomly created.</returns>
        [NotNull, ItemNotNull]
        protected abstract ImmutableList<Individual<T>> Initialization();

        /// <summary>
        /// The crossovers algorithm.
        /// </summary>
        /// <param name="mostFitParent">The most fit parent.</param>
        /// <param name="lessFitParent">The less fit parent.</param>
        /// <remarks>While not all algorithms are requiring a distinction between most and less fit parents,
        /// the Population class is always placing them in this order in case the algorithm could use this to produce better
        /// descendents (the BLX alpha-beta algorithm, for example)</remarks>
        /// <returns>A new genome that is the result of applying the crossover on both parents.</returns>
        [NotNull]
        protected abstract ImmutableList<T> Crossover([NotNull] ImmutableList<T> mostFitParent, [NotNull] ImmutableList<T> lessFitParent);

        /// <summary>
        /// The mutations algorithm.
        /// </summary>
        /// <param name="genome">The genome that will be mutated.</param>
        /// <returns>A new genome, obtained through mutation of the original one.</returns>
        [NotNull]
        protected abstract ImmutableList<T> Mutation([NotNull] ImmutableList<T> genome);

        /// <summary>
        /// The selections algorithm.
        /// </summary>
        /// <param name="individuals">The individuals to select.</param>
        /// <param name="fitnesses">The fitnesses of the individuals.</param>
        /// <returns>A list with the individuals selected. These individuals will be used to generate the next population</returns>
        [NotNull]
        protected abstract Queue<IndividualsWithFitness<T>> Selection(
            [NotNull] ImmutableList<IndividualsWithFitness<T>> individuals,
            [NotNull] double[] fitnesses);

        /// <summary>
        /// The replacements algorithm.
        /// </summary>
        /// <param name="oldPopulation">The old population.</param>
        /// <param name="newBorns">The new borns.</param>
        /// <returns>The new population, its composition depends on the chosen algorithm</returns>
        [NotNull, ItemNotNull]
        protected abstract ImmutableList<Individual<T>> Replacement(
            [NotNull] ImmutableList<Individual<T>> oldPopulation,
            [NotNull] ImmutableList<Individual<T>> newBorns);

        /// <summary>
        /// Calculates the genetic diversity between two individuals.
        /// </summary>
        /// <param name="individual1">The first indivdual.</param>
        /// <param name="individual2">The second individual.</param>
        /// <returns>A double value that represent the distance between two genomes. </returns>
        /// <exception cref="NotImplementedException">As the calculation of the diversity it is required by the configuration, it is necessary to implement the GeneticDiversity method.</exception>
        protected virtual double CalculateGeneticDiversity([NotNull] Individual<T> individual1, [NotNull] Individual<T> individual2)
        {
            throw new NotImplementedException("if the calculation of the diversity is required by the configuration, " +
                "it is necessary to implement the GeneticDiversity method.");
        }
    }
}