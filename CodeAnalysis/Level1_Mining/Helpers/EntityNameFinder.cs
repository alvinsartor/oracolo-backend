﻿using System.IO;
using System.Linq;
using Extensions;
using JetBrains.Annotations;
using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using System.Collections.Immutable;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeAnalysis.Level0_Mapping.Helpers
{
    internal sealed class EntityNameFinder
    {
        /// <summary>
        /// Extracts the entity name from the specified file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        [NotNull]
        public static ImmutableList<string> ExtractEntityNameFromFile([NotNull] string filePath)
        {
            filePath.AssertNotNull(nameof(filePath));

            string fileContent = File.ReadAllText(filePath);
            SyntaxTree tree = CSharpSyntaxTree.ParseText(fileContent);

            IEnumerable<SyntaxNode> namespaces = tree.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>();

            IEnumerable<string> entities =
                from namespaceNode in namespaces
                let namespaceString = ExtractNamespace(namespaceNode)
                from declaration in ExtractDeclarations(namespaceNode)
                select namespaceString + declaration;

            return entities.ToImmutableList();
        }

        [NotNull]
        private static string ExtractNamespace([NotNull] SyntaxNode namespaceDeclaration)
        {
            return namespaceDeclaration.ChildNodes().First(x => x is QualifiedNameSyntax).ToString();
        }

        [NotNull]
        private static ImmutableList<string> ExtractDeclarations([NotNull] SyntaxNode namespaceDeclaration)
        {
            return namespaceDeclaration.ChildNodes()
                .OfType<TypeDeclarationSyntax>().Select(x => x.Identifier.ToString())
                .ToImmutableList();
        }
    }
}
