﻿using NUnit.Framework;

namespace CodeAnalysis.Mapper.Helpers.NUnit
{
    [TestFixture]
    internal sealed class DllNameAndPathFixture
    {
        [Test]
        public void ClassIsInitializedAsExpected()
        {
            var dllNameAndPath = new DllNameAndPath("foo", $@"foo\bar\foo.dll");

            Assert.That(dllNameAndPath.Name, Is.EqualTo("foo"));
            Assert.That(dllNameAndPath.Path, Is.EqualTo($@"foo\bar\foo.dll"));
        }
    }
}
