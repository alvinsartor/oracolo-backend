﻿using NUnit.Framework;

namespace CodeAnalysis.Level2_Extraction.Helpers.NUnit
{
    [TestFixture]
    internal sealed class SyntaxTreeAnaliserFixture
    {
        //private static (SyntaxNode, SemanticModel) ExtractFirstEntity([NotNull] string fullClass)
        //{
        //    SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(fullClass);
        //    Compilation compilation = CSharpCompilation.Create("TestCompilation")
        //        .AddSyntaxTrees(syntaxTree)
        //        .AddReferences(ReferencesLoader.CreateReferencesFromFiles(ReferencesLoader.GetBaseReferences()));

        //    SyntaxNode syntaxNode = syntaxTree.GetRoot().DescendantNodes().OfType<TypeDeclarationSyntax>().First();

        //    return (syntaxNode, compilation.GetSemanticModel(syntaxTree));
        //}

        //[TestCase("class Foo { }", "Foo")]
        //[TestCase("class Foo<T> { }", "Foo<T>")]
        //[TestCase("class Foo<Bar> { } class Bar { }", "Foo<Bar>")]
        //[TestCase("namespace A.B.C { class Foo { } }", "A.B.C.Foo")]
        //[TestCase("namespace A.B.C { class Foo<T> { } }", "A.B.C.Foo<T>")]
        //[TestCase("namespace A.B.C { class Foo<Bar> { } class Bar { }}", "A.B.C.Foo<Bar>")]
        //public void EntityFullNameIsRetreived([NotNull] string classContent, [NotNull] string expectedResult)
        //{
        //    (SyntaxNode syntaxNode, SemanticModel semanticModel) = ExtractFirstEntity(classContent);

        //    string name = SyntaxTreeAnaliser.ExtractSyntaxNodeFullName(syntaxNode, semanticModel);
        //    Assert.That(name, Is.EqualTo(expectedResult));
        //}

        //[TestCase("class Foo { }")]
        //[TestCase("class Foo : Bar { } interface Bar {}", "Bar")]
        //[TestCase("class Foo : Bar, Baz { } interface Bar {} interface Baz {}", "Bar", "Baz")]
        //[TestCase("class Foo<T> : Bar<T> { } interface Bar<T> {}", "Bar<T>")]
        //[TestCase("class Foo<T> : Bar<TInput> { } interface Bar<T> {}", "Bar<T>")]
        //[TestCase("namespace A.B.C { class Foo : Bar {} interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo : Bar, Baz { } interface Bar {} interface Baz {} }", "A.B.C.Bar", "A.B.C.Baz")]
        //[TestCase("namespace A.B.C { class Foo<T> : Bar<T> {} interface Bar<T> {} }", "A.B.C.Bar<T>")]
        //[TestCase("namespace A.B.C { class Foo<T> : Bar<Baz<T>> {} interface Bar<T> {} interface Baz<T> {} }", "A.B.C.Bar<T>")]
        ////public void EntityInheritancesAreFetched([NotNull] string classContent, [NotNull] params string[] expectedResult)
        ////{
        ////    (SyntaxNode syntaxNode, SemanticModel semanticModel) = ExtractFirstEntity(classContent);

        ////    ImmutableList<string> inheritances = SyntaxTreeAnaliser.GetInheritances(syntaxNode, semanticModel);
        ////    CollectionAssert.AreEquivalent(expectedResult.ToImmutableList(), inheritances);
        ////}

        //// no dependencies
        //[TestCase("class Foo { }")]
        //[TestCase("class Foo : Bar { } interface Bar { }")]

        //// d. in variables
        //[TestCase("class Foo { Bar bar; } interface Bar {}", "Bar")]
        //[TestCase("class Foo { Bar[] bar; } interface Bar {}", "Bar")]
        //[TestCase("class Foo { Bar bar; Baz baz; } interface Bar {} interface Baz {}", "Bar", "Baz")]
        //[TestCase("namespace A.B.C { class Foo { Bar bar; } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo { Bar[] bar; } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo { Bar bar; Baz baz; } interface Bar {} interface Baz {} }", "A.B.C.Bar", "A.B.C.Baz")]

        //// d. in constructor
        //[TestCase("class Foo{ public Foo(Bar bar) {} } interface Bar {}", "Bar")]
        //[TestCase("class Foo{ public Foo() { Bar bar; } } interface Bar {}", "Bar")]
        //[TestCase("class Foo{ public Foo() { Bar[] bar; } } interface Bar {}", "Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public Foo(Bar bar) {} } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public Foo() { Bar bar; } } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public Foo() { Bar[] bar; } } interface Bar {} }", "A.B.C.Bar")]

        //// d. in methods
        //[TestCase("class Foo{ public void Method(Bar bar) {} } interface Bar {}", "Bar")]
        //[TestCase("class Foo{ public Bar Method() {} } interface Bar {}", "Bar")]
        //[TestCase("class Foo{ public void Method() { Bar bar; } } interface Bar {}", "Bar")]
        //[TestCase("class Foo{ public void Method() { Bar[] bar; } } interface Bar {}", "Bar")]
        //[TestCase("class Foo{ public void Method1() {} public void Method2() { Bar bar; } } interface Bar {}", "Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public void Method(Bar bar) {} } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public Bar Method() {} } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public void Method() { Bar bar; } } interface Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public void Method() { Bar[] bar; } } interface Bar {} }", "A.B.C.Bar")]

        //// d. in properties
        //[TestCase("class Foo{ public Bar FooFoo => new Bar(); } class Bar {}", "Bar")]
        //[TestCase("class Foo{ public string FooFoo => new Bar().ToString(); } class Bar {}", "Bar", "string")]
        //[TestCase("class Foo { public string FooFoo { get { return new Bar().ToString(); } } } class Bar { }", "Bar", "string")]
        //[TestCase("namespace A.B.C { class Foo{ public Bar FooFooFoo => new Bar(); } class Bar {} }", "A.B.C.Bar")]
        //[TestCase("namespace A.B.C { class Foo{ public string FooFoo => new Bar().ToString(); } class Bar {} }", "A.B.C.Bar", "string")]

        //// d. in generic inheritance
        //[TestCase("class Foo<T> : Bar<T> { } interface Bar<T> {}", "T")]
        //[TestCase("namespace A.B.C { class Foo<T> : Bar<Baz<T>> {} interface Bar<T> {} interface Baz<T> {} }", "A.B.C.Baz<T>", "T")]
        //public void EntityDependenciesAreFetched([NotNull] string classContent, [NotNull] params string[] expectedResult)
        //{
        //    (SyntaxNode syntaxNode, SemanticModel semanticModel) = ExtractFirstEntity(classContent);

        //    ImmutableList<string> dependencies = SyntaxTreeAnaliser.GetDependencies(syntaxNode, semanticModel);
        //    CollectionAssert.AreEquivalent(expectedResult.ToImmutableList(), dependencies);
        //}
    }
}