﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Extensions.NUnit
{
    [TestFixture]
    internal sealed class EnumerableExtensionsFixture
    {
        [Test]
        public void ForEachExecutesTheAction()
        {
            IEnumerable<int> collection = new[] {1, 2, 3, 4};

            var counter = 0;
            collection.ForEach(x => counter += x);

            Assert.That(counter == 10);
        }

        [Test]
        public void ForEachThrowsWhenCollectionIsNull()
        {
            IEnumerable<int> collection = null;

            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.Throws<ArgumentException>(() => collection.ForEach(x =>
            {
                int _ = x;
            }));
        }

        [Test]
        public void ForEachThrowsWhenActionIsNull()
        {
            IEnumerable<int> collection = new[] {1, 2, 3, 4};

            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.Throws<ArgumentException>(() => collection.ForEach(null));
        }

        [Test]
        public void ForEachDoesNotThrowWhenCollectionIsEmpty()
        {
            IEnumerable<int> collection = new int [0];
            var counter = 0;
            Assert.DoesNotThrow(() => collection.ForEach(x => counter++));

            Assert.That(counter, Is.EqualTo(0));
        }

        [Test]
        public void DivideReturnsTheExpectedCollections()
        {
            IEnumerable<int> collection = new[] {0, 1, 2, 3, 4, 5};
            (IEnumerable<int> even, IEnumerable<int> odd) = collection.Divide(x => x % 2 == 0);

            CollectionAssert.AreEquivalent(even, new[] {0, 2, 4});
            CollectionAssert.AreEquivalent(odd, new[] {1, 3, 5});
        }

        [Test]
        public void MaxByTest()
        {
            var rnd = new Random();
            const int TimesToRepeatTest = 1000;
            const int listSize = 10;

            var list = new List<double>();

            for (var i = 0; i < TimesToRepeatTest; i++)
            {
                list.Clear();
                for (var k = 0; k < listSize; k++)
                {
                    list.Add(rnd.NextDouble());
                }

                IEnumerable<int> indexes = list.Select((v, index) => index);

                int indexOfTheHighest = indexes.ToList().MaxBy(x => list[x]);

                Assert.That(list[indexOfTheHighest], Is.EqualTo(list.Max()));
            }
        }

        [Test]
        public void MinByTest()
        {
            var rnd = new Random();
            const int TimesToRepeatTest = 1000;
            const int listSize = 10;

            var list = new List<double>();

            for (var i = 0; i < TimesToRepeatTest; i++)
            {
                list.Clear();
                for (var k = 0; k < listSize; k++)
                {
                    list.Add(rnd.NextDouble());
                }

                IEnumerable<int> indexes = list.Select((v, index) => index);

                int indexOfTheHighest = indexes.ToList().MinBy(x => list[x]);

                Assert.That(list[indexOfTheHighest], Is.EqualTo(list.Min()));
            }
        }
    }
}