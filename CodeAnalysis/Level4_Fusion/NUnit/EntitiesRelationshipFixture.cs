﻿using NUnit.Framework;

namespace CodeAnalysis.Level4_Fusion.NUnit
{
    [TestFixture]
    internal sealed class EntitiesRelationshipFixture
    {
        //private readonly List<EntityInfo> m_Entities1 = new List<EntityInfo>(new[]
        //{
        //    new EntityInfo($@"A\B\C\Batman.cs", "A.B.C.Batman", new List<string>(), new List<string>(new[] {"A.B.C.Alfred"})),
        //    new EntityInfo($@"A\B\C\Batman.cs", "A.B.C.Alfred", new List<string>(), new List<string>()),
        //    new EntityInfo($@"A\B\C\Robin.cs", "A.B.C.Robin", new List<string>(new[] {"A.B.C.Batman"}), new List<string>()),
        //    new EntityInfo($@"A\B\C\Catwoman.cs", "A.B.C.Catwoman", new List<string>(), new List<string>(new[] {"A.B.C.Catwoman"})),
        //});

        //private readonly List<EntityInfo> m_Entities2 = new List<EntityInfo>(new[]
        //{
        //    new EntityInfo($@"A\B\C\Jocker.cs", "A.B.C.Jocker", new List<string>(), new List<string>(new[] {"A.B.C.Batman"})),
        //    new EntityInfo($@"A\B\C\Penguin.cs", "A.B.C.Penguin", new List<string>(new[] {"A.B.C.Batman"}), new List<string>(new[] {"A.B.C.Batman"})),
        //    new EntityInfo($@"A\B\C\TwoFaces.cs", "A.B.C.TwoFaces", new List<string>(), new List<string>()),
        //    new EntityInfo($@"A\B\C\PoisonIvy.cs", "A.B.C.PoisonIvy", new List<string>(new[] {"A.B.C.Catwoman"}), new List<string>()),
        //});

        //[Test]
        //public void EntitiesRelationshipsAreCreatedCorrectly()
        //{
        //    var entitiesRelationship = new EntitiesRelationshipCache();

        //    // checking entities
        //    CollectionAssert.AreEquivalent(entitiesRelationship.Entities.Keys, m_Entities1.Select(x => x.FullName));

        //    IEnumerable<INode<EntityDescription>> dependencies;
        //    IEnumerable<INode<EntityDescription>> inheritances;

        //    (inheritances, dependencies) = entitiesRelationship["A.B.C.Batman"];
        //    Assert.IsEmpty(inheritances);
        //    Assert.That(dependencies.First().Value.FullName, Is.EqualTo("A.B.C.Alfred"));

        //    (inheritances, dependencies) = entitiesRelationship["A.B.C.Alfred"];
        //    Assert.IsEmpty(dependencies);
        //    Assert.IsEmpty(inheritances);

        //    (inheritances, dependencies) = entitiesRelationship["A.B.C.Robin"];
        //    Assert.That(inheritances.First().Value.FullName, Is.EqualTo("A.B.C.Batman"));
        //    Assert.IsEmpty(dependencies);

        //    (inheritances, dependencies) = entitiesRelationship["A.B.C.Catwoman"];
        //    Assert.IsEmpty(inheritances);
        //    Assert.That(dependencies.First().Value.FullName, Is.EqualTo("A.B.C.Catwoman"));
        //}
    }

    
}