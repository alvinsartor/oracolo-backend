﻿using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph
{
    public sealed class Edge<T> : IEdge<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Edge{T}"/> class.
        /// </summary>
        /// <param name="fromNode">Node the edge is starting from.</param>
        /// <param name="toNode">Node the edge is arriving to.</param>
        /// <param name="weight">The weight of the edge.</param>
        internal Edge([NotNull] INode<T> fromNode, [NotNull] INode<T> toNode, double weight = 1)
        {
            Weight = weight;
            StartNode = fromNode.NotNull(nameof(fromNode));
            DestinationNode = toNode.NotNull(nameof(toNode));
        }

        /// <inheritdoc />
        public INode<T> DestinationNode { get; }

        /// <inheritdoc />
        public INode<T> StartNode { get; }

        /// <inheritdoc />
        public double Weight { get; }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            return other is IEdge<T> edge
                   && Equals(edge);
        }

        /// <inheritdoc />
        public bool Equals(IEdge<T> other)
        {
            return other != null 
                   && DestinationNode.Id.Equals(other.DestinationNode.Id)
                   && StartNode.Id.Equals(other.StartNode.Id)
                   && Weight.Equals(other.Weight);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = DestinationNode.GetHashCode();
                hashCode = (hashCode * 397) ^ StartNode.GetHashCode();
                hashCode = (hashCode * 397) ^ Weight.GetHashCode();
                return hashCode;
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"'{StartNode}' -> '{DestinationNode}', [w:{Weight}]";
        }
    }
}
