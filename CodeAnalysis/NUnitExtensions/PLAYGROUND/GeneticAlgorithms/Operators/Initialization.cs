﻿using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Components;
using Fugro.GeneticAlgorithms.Util;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Operators
{
    /// <summary>
    /// The initialization algorithms are used to create a new initial population.
    ///
    /// <para>These are standard initialization algorithms.
    ///
    /// As these algorithms are only for certain types,
    /// the user may write a personalised algorithm that fits better with the problem.
    /// </para>
    /// </summary>
    public static class Initialization
    {
        /// <summary>
        /// Initializes a binary population.
        /// </summary>
        /// <param name="genomeSize">Size of the genomes.</param>
        /// <param name="populationSize">Size of the population.</param>
        /// <returns>A new population with ranodm binary genomes</returns>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull, ItemNotNull]
        public static ImmutableList<Individual<bool>> BinaryInitialization(int genomeSize, int populationSize)
        {
            var population = new List<Individual<bool>>();

            while (population.Count < populationSize)
            {
                var genome = new bool[genomeSize];
                for (var i = 0; i < genomeSize; i++)
                {
                    genome[i] = Random.GetRandomBool();
                }

                population.Add(new Individual<bool>(genome));
            }

            return population.ToImmutableList();
        }

        /// <summary>
        /// Initializes a population with real number genomes.
        /// </summary>
        /// <param name="genomeSize">Size of the genomes.</param>
        /// <param name="populationSize">Size of the population.</param>
        /// <param name="minBoundary">The min value for each gene</param>
        /// <param name="maxBoundary">The max value for each gene</param>
        /// <returns>A new population with random real number genomes</returns>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull, ItemNotNull]
        public static ImmutableList<Individual<double>> RealInitialization(int genomeSize, int populationSize, double minBoundary, double maxBoundary)
        {
            var population = new List<Individual<double>>();

            while (population.Count < populationSize)
            {
                var genome = new double[genomeSize];
                for (var i = 0; i < genomeSize; i++)
                {
                    genome[i] = Random.GetRandomInRange(minBoundary, maxBoundary);
                }

                population.Add(new Individual<double>(genome));
            }

            return population.ToImmutableList();
        }

        /// <summary>
        /// Initializes a population with real number genomes using a parameters list.
        /// </summary>
        /// <param name="populationSize">Size of the population.</param>
        /// <param name="parameters">A list of parameters. The maximum and minimum values of each list will be extracted and used
        /// as boundaries for the relative gene</param>
        /// <returns>A new population with random real number genomes</returns>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull, ItemNotNull]
        public static ImmutableList<Individual<double>> RealInitialization(int populationSize,
            [NotNull] ImmutableList<ImmutableList<double>> parameters)
        {
            ArgumentHelper.AssertNotNull(parameters, nameof(parameters));
            var population = new List<Individual<double>>();

            while (population.Count < populationSize)
            {
                double[] genome = parameters.Select(list => Random.GetRandomInRange(list.Min(), list.Max())).ToArray();
                population.Add(new Individual<double>(genome));
            }

            return population.ToImmutableList();
        }

        /// <summary>
        /// Initializes a population with objects ordered randomly.
        /// </summary>
        /// <param name="populationSize">Size of the population.</param>
        /// <param name="objects">List of all possible objects</param>
        /// <returns>A new population with positional genomes</returns>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull, ItemNotNull]
        public static ImmutableList<Individual<T>> PositionalInitialization<T>(int populationSize, [NotNull] IList<T> objects)
        {
            ArgumentHelper.AssertNotNull(objects, nameof(objects));
            var population = new List<Individual<T>>();

            while (population.Count < populationSize)
            {
                IList<T> genome = objects.ShuffledCopy();
                population.Add(new Individual<T>(genome));
            }

            return population.ToImmutableList();
        }
    }
}