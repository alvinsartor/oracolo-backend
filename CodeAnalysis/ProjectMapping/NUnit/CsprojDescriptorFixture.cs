﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using CodeAnalysis.Mapper.Helpers;
using CodeAnalysis.ProjectMapping.NUnitExtensions;
using NUnit.Framework;

namespace CodeAnalysis.ProjectMapping.NUnit
{
    [TestFixture]
    internal sealed class CsprojDescriptorFixture
    {
        [Test]
        public void CsprojDescriptorIsInitializedCorrectly()
        {
            var testSln = new TestSlnDescriptor();
            var nameAndPath = new CsprojNameAndPath("Bull", $@"c:\ProjectStructure\FooSolution\BullProject\Bull.csproj");
            string OutputPath(string csprojName) => $@"c:\ProjectStructure\Output\{csprojName}.dll";

            string CsprojOutputPathFinderFunc(string csprojPath, string csprojName) => OutputPath(csprojName);               

            ImmutableList<string> CsharpFinderFunc(string s) => new List<string>
            {
                Path.Combine(Path.GetDirectoryName(s) ?? throw new Exception("Parent not found"), "Files", "class1.cs"),
                Path.Combine(Path.GetDirectoryName(s) ?? throw new Exception("Parent not found"), "Files", "class2.cs"),
                Path.Combine(Path.GetDirectoryName(s) ?? throw new Exception("Parent not found"), "Files", "class3.cs")
            }.ToImmutableList();

            ImmutableList<DllNameAndPath> ReferencesFinderFunc(string s) => new List<DllNameAndPath>
            {
                new DllNameAndPath("BarProject", OutputPath("BarProject")),
                new DllNameAndPath("OtherReference", OutputPath("BarProject"))
            }.ToImmutableList();


            var csprojDescriptor = new CsprojDescriptor(nameAndPath, testSln, CsprojOutputPathFinderFunc, CsharpFinderFunc, ReferencesFinderFunc);
            
            // sln descriptor
            Assert.That(csprojDescriptor.SlnDescriptor.FilePath, Is.EqualTo(testSln.FilePath));

            // csproj descriptor
            Assert.That(csprojDescriptor.FilePath, Is.EqualTo(nameAndPath.Path));
            Assert.That(csprojDescriptor.CsprojName, Is.EqualTo(nameAndPath.Name));
            Assert.That(csprojDescriptor.OutputPath, Is.EqualTo($@"c:\ProjectStructure\Output\Bull.dll"));

            // files descriptors
            Assert.That(csprojDescriptor.Files.Count, Is.EqualTo(3));
            Assert.That(csprojDescriptor.Files[0].FileName, Is.EqualTo("class1.cs"));
            Assert.That(csprojDescriptor.Files[1].FileName, Is.EqualTo("class2.cs"));
            Assert.That(csprojDescriptor.Files[2].FileName, Is.EqualTo("class3.cs"));

            // references descriptors
            Assert.That(csprojDescriptor.References.Count, Is.EqualTo(2));
            Assert.That(csprojDescriptor.References[0].AssemblyName, Is.EqualTo("BarProject"));
            Assert.That(csprojDescriptor.References[1].AssemblyName, Is.EqualTo("OtherReference"));
        }
    }
}
