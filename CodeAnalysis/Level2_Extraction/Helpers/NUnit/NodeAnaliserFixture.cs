﻿using NUnit.Framework;

namespace CodeAnalysis.Level2_Extraction.Helpers.NUnit
{
    [TestFixture]
    internal sealed class NodeAnaliserFixture
    {
        ///// <summary>
        ///// Extracts the first variable declaration element from a string containing one or more entities.
        ///// </summary>
        //private static (SyntaxNode firstInheritanceNode, SemanticModel model) ExtractFirstVariableDeclarationElement([NotNull] string fullClass)
        //{
        //    SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(fullClass);
        //    Compilation compilation = CSharpCompilation.Create("TestCompilation")
        //        .AddSyntaxTrees(syntaxTree)
        //        .AddReferences(ReferencesLoader.CreateReferencesFromFiles(ReferencesLoader.GetBaseReferences()));

        //    SyntaxNode firstInheritanceNode = syntaxTree.GetRoot()
        //        .DescendantNodes().OfType<TypeDeclarationSyntax>().First()
        //        .ChildNodes().OfType<FieldDeclarationSyntax>().Single()
        //        .ChildNodes().OfType<VariableDeclarationSyntax>().First()
        //        .ChildNodes().First();

        //    return (firstInheritanceNode, compilation.GetSemanticModel(syntaxTree));
        //}

        //[Test] // Test on the fixture function
        //public void TheFirstDeclarationElementIsExtracted()
        //{
        //    const string entity = "namespace A.B.C { class Foo<T> { Bar<T> bar; } interface Bar<T> {} }";
        //    SyntaxNode firstInheritanceNode = ExtractFirstVariableDeclarationElement(entity).firstInheritanceNode;

        //    Assert.That(firstInheritanceNode.ToString(), Is.EqualTo("Bar<T>"));
        //}

        //[TestCase("class Foo { Bar bar; } interface Bar {}", "Bar")]
        //[TestCase("namespace A.B.C { class Foo { Bar bar; } interface Bar {} }", "A.B.C.Bar")]
        //public void SimpleNodesTypesAreHandled([NotNull] string entity, [NotNull] params string[] expectedResult)
        //{
        //    (SyntaxNode firstInheritanceNode, SemanticModel model) = ExtractFirstVariableDeclarationElement(entity);
        //    ImmutableList<string> definitons = NodeAnaliser.GetNodeOriginalDefinitions(firstInheritanceNode, model);

        //    CollectionAssert.AreEquivalent(expectedResult.ToImmutableList(), definitons);
        //}

        //[TestCase("namespace A.B.C { class Foo<T> { Bar<T> bar; } interface Bar<T> {} }", "A.B.C.Bar<T>", "T")]
        //[TestCase("namespace A.B.C { class Foo<T> { Bar<Baz<T>> bar; } class Bar<T> {} class Baz<T> {} }", "A.B.C.Bar<T>", "A.B.C.Baz<T>", "T")]
        //[TestCase("class Foo<T> { Bar<T, T> bar; } interface Bar<T, T1> {}", "Bar<T, T1>", "T")]
        //[TestCase("class Foo { Bar<double, double> bar; } interface Bar<T, T1> {}", "Bar<T, T1>", "double")]
        //[TestCase("class Foo { Bar<double, string> bar; } interface Bar<T, T1> {}", "Bar<T, T1>", "double", "string")]
        //[TestCase("class Foo { Bar<double, Bar<double, double>> bar; } interface Bar<T, T1> {}", "Bar<T, T1>", "double")]
        //public void GenericNodesTypesAreHandled([NotNull] string entity, [NotNull] params string[] expectedResult)
        //{
        //    (SyntaxNode firstInheritanceNode, SemanticModel model) = ExtractFirstVariableDeclarationElement(entity);
        //    ImmutableList<string> definitons = NodeAnaliser.GetNodeOriginalDefinitions(firstInheritanceNode, model);

        //    CollectionAssert.AreEquivalent(expectedResult.ToImmutableList(), definitons);
        //}

        //[TestCase("class Foo { (Bar, double) bar; } interface Bar {}", "Bar", "double")]
        //[TestCase("class Foo { (Bar bar, double bim) baz; } interface Bar {}", "Bar", "double")]
        //[TestCase("namespace A.B.C { class Foo { (Bar, double) bar; } interface Bar {} }", "A.B.C.Bar", "double")]
        //public void TupleNodesAreHandled([NotNull] string entity, [NotNull] params string[] expectedResult)
        //{
        //    (SyntaxNode firstInheritanceNode, SemanticModel model) = ExtractFirstVariableDeclarationElement(entity);
        //    ImmutableList<string> definitons = NodeAnaliser.GetNodeOriginalDefinitions(firstInheritanceNode, model);

        //    CollectionAssert.AreEquivalent(expectedResult.ToImmutableList(), definitons);
        //}
    }
}