﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using JetBrains.Annotations;

namespace Extensions
{
    public sealed class ImmutableLookup<TKey, TValue> : ILookup<TKey, TValue>
    {
        private readonly ImmutableDictionary<TKey, ImmutableList<TValue>> m_Cache;

        /// <summary>
        /// Gets an empty instance of the <see cref="ImmutableLookup{TKey, TValue}"/> class.
        /// </summary>
        [NotNull]
        public static ImmutableLookup<TKey, TValue> Empty =>
            new ImmutableLookup<TKey, TValue>(ImmutableDictionary<TKey, ImmutableList<TValue>>.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="ImmutableLookup{TKey, TValue}"/> class.
        /// </summary>
        /// <param name="values">The values.</param>
        private ImmutableLookup([NotNull] ImmutableDictionary<TKey, ImmutableList<TValue>> values)
        {
            m_Cache = values;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImmutableLookup{TKey, TValue}"/> class.
        /// </summary>
        /// <param name="values">The values.</param>
        private ImmutableLookup([NotNull] IDictionary<TKey, ImmutableList<TValue>> values)
        {
            m_Cache = values.ToImmutableDictionary();
        }

        /// <inheritdoc />
        public IEnumerator<IGrouping<TKey, TValue>> GetEnumerator()
        {
            return m_Cache.Keys.SelectMany(k => m_Cache[k].GroupBy(_ => k)).GetEnumerator();
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <inheritdoc />
        public bool Contains(TKey key)
        {
            return m_Cache.ContainsKey(key);
        }

        /// <inheritdoc />
        public int Count => m_Cache.Count;

        /// <summary>
        /// Gets the number of elements in the Lookup. This has complexity of O(keys).
        /// </summary>        
        public int CountValues => m_Cache.Sum(x => x.Value.Count);

        /// <inheritdoc />
        public IEnumerable<TValue> this[TKey key] => m_Cache[key];

        /// <summary>
        /// Returns a new instance of ImmutableLookUp with the new value.
        /// </summary>
        /// <remarks>The complexity of the operation is O(logN)</remarks>
        /// <param name="key">The key.</param>
        /// <param name="element">The element to be added.</param>
        [NotNull, Pure]
        public ImmutableLookup<TKey, TValue> Add(TKey key, TValue element)
        {
            return m_Cache.ContainsKey(key)
                ? m_Cache[key].Contains(element)
                    ? throw new ArgumentException($"The element '{element}' is already in the lookup.")
                    : new ImmutableLookup<TKey, TValue>(m_Cache.SetItem(key, m_Cache[key].Add(element)))
                : new ImmutableLookup<TKey, TValue>(m_Cache.Add(key, ImmutableList<TValue>.Empty.Add(element)));
        }

        /// <summary>
        /// Returns a new instance of ImmutableLookUp with the new values.
        /// </summary>
        /// <remarks>The complexity of the operation is O(logN*K + N),
        /// where N are the element already in the dictionary and K are the elements that will be added.</remarks>
        /// <param name="elements">The elements.</param>
        /// <param name="getKeyFunction">The get key function.</param>
        [NotNull, Pure]
        public ImmutableLookup<TKey, TValue> AddRange(
            [NotNull] IList<TValue> elements,
            [NotNull] Func<TValue, TKey> getKeyFunction)
        {
            Dictionary<TKey, ImmutableList<TValue>> copy = new Dictionary<TKey, ImmutableList<TValue>>(m_Cache);
            foreach (var element in elements)
            {
                TKey key = getKeyFunction(element);
                if (copy.ContainsKey(key))
                {
                    copy[key] = copy[key].Contains(element)
                        ? throw new ArgumentException($"The element '{element}' is already in the lookup.")
                        : copy[key].Add(element);
                }
                else
                {
                    copy.Add(key, ImmutableList<TValue>.Empty.Add(element));
                }                
            }

            return new ImmutableLookup<TKey, TValue>(copy);
        }
    }
}