﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Util.NUnit
{
    [TestFixture]
    internal sealed class ExtensionFixture
    {
        private struct StructWithId
        {
            public readonly int Id;
            public readonly double Value;

            public StructWithId(int id, double value)
            {
                Id = id;
                Value = value;
            }
        }

        [Test]
        public void CumulativeSumTest()
        {
            var array = new[] { 1d, 2d, 2d, 5d, 8d, 2d };

            var expectedResult = new[] { 1d, 3d, 5d, 10d, 18d, 20d };

            double[] result = array.CumulativeSum().ToArray();

            Assert.That(result, Is.EqualTo(expectedResult));
        }


        [Test]
        public void MaxByReturnsTheMaximumElementOfAList()
        {
            const int TimesToRepeatTest = 1000;
            const int listSize = 10;

            var list = new List<double>();

            for (var i = 0; i < TimesToRepeatTest; i++)
            {
                list.Clear();
                for (var k = 0; k < listSize; k++)
                {
                    list.Add(Random.GetRandomDouble() * (Random.GetRandomDouble() - 1));
                }

                IList<int> indexes = list.Select((v, index) => index).ToList();
                int indexOfTheMax = indexes.MaxBy(x => list[x]);

                Assert.That(list[indexOfTheMax], Is.EqualTo(list.Max()));
            }
        }

        [Test]
        public void MaxByOnAListWithMultipleOccurrencesOfMaxReturnsTheFirstElement()
        {
            var list = new List<StructWithId>(new[]
            {
                new StructWithId(1, 15),
                new StructWithId(2, 7),
                new StructWithId(3, 2),
                new StructWithId(4, 1),
                new StructWithId(5, 15),
            });

            StructWithId maxOfList = list.MaxBy(x => x.Value);

            Assert.That(maxOfList.Id, Is.EqualTo(1));
        }

        [Test]
        public void MaxByThrowsIfCollectionIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => new List<int>().MaxBy(x => x));
        }

        [Test]
        public void MinByReturnsTheMinimumElementOfAList()
        {
            const int TimesToRepeatTest = 1000;
            const int listSize = 10;

            var list = new List<double>();

            for (var i = 0; i < TimesToRepeatTest; i++)
            {
                list.Clear();
                for (var k = 0; k < listSize; k++)
                {
                    list.Add(Random.GetRandomDouble() * (Random.GetRandomDouble() - 1));
                }

                IList<int> indexes = list.Select((v, index) => index).ToList();
                int indexOfTheMin = indexes.MinBy(x => list[x]);

                Assert.That(list[indexOfTheMin], Is.EqualTo(list.Min()));
            }
        }

        [Test]
        public void MinByOnAListWithMultipleOccurrencesOfMinReturnsTheFirstElement()
        {
            var list = new List<StructWithId>(new[]
            {
                new StructWithId(1, 15),
                new StructWithId(2, 7),
                new StructWithId(3, 2),
                new StructWithId(4, 2),
                new StructWithId(5, 15),
            });

            StructWithId minOfList = list.MinBy(x => x.Value);

            Assert.That(minOfList.Id, Is.EqualTo(3));
        }

        [Test]
        public void MinByThrowsIfCollectionIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => new List<int>().MinBy(x => x));
        }

        [Test]
        public void ShuffledTest()
        {
            const int TimesToRepeatTest = 1000;
            const int listSize = 10;

            var list = new List<double>();

            for (var i = 0; i < TimesToRepeatTest; i++)
            {
                list.Clear();
                for (var k = 0; k < listSize; k++)
                {
                    list.Add(Random.GetRandomDouble());
                }

                IList<double> shuffledList = list.ShuffledCopy();

                Assert.That(list, Is.Not.EqualTo(shuffledList));
                Assert.That(list.All(x => shuffledList.Contains(x)));
            }
        }

        [Test]
        public void SwapReturnsExpectedValues()
        {
            IList<int> list = new List<int>(new[] { 1, 2, 3 });

            list.Swap(0, 1);
            Assert.That(list, Is.EqualTo(new List<int>(new[] { 2, 1, 3 })));

            list.Swap(0, 1);
            Assert.That(list, Is.EqualTo(new List<int>(new[] { 1, 2, 3 })));

            list.Swap(1, 1);
            Assert.That(list, Is.EqualTo(new List<int>(new[] { 1, 2, 3 })));

            list.Swap(1, 2);
            Assert.That(list, Is.EqualTo(new List<int>(new[] { 1, 3, 2 })));

            Assert.Throws<ArgumentOutOfRangeException>(() => list.Swap(-1, 1));
            Assert.Throws<ArgumentOutOfRangeException>(() => list.Swap(-1, 3));
            Assert.Throws<ArgumentOutOfRangeException>(() => list.Swap(1, 3));
        }
    }
}
