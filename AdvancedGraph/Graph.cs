﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph
{
    public class Graph<T> : IGraph<T>
    {
        [NotNull] private readonly Dictionary<T, Node<T>> m_Nodes;
        [NotNull] private readonly Dictionary<Guid, Node<T>> m_NodesIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="Graph{T}"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Graph([CanBeNull] string name = null)
        {
            Id = Guid.NewGuid();
            Name = name ?? $"graph {Id.ToString()}";
            m_Nodes = new Dictionary<T, Node<T>>();
            m_NodesIds = new Dictionary<Guid, Node<T>>();
        }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public Guid Id { get; }

        /// <inheritdoc />
        public int Count => m_Nodes.Count;

        /// <summary>
        /// Gets the nodes of this graph.
        /// <remarks>Note: This call has complexity O(#Nodes).</remarks>
        /// </summary>
        public ImmutableHashSet<INode<T>> Nodes => m_Nodes.Values.Cast<INode<T>>().ToImmutableHashSet();

        /// <inheritdoc />
        public ImmutableHashSet<T> Values => m_Nodes.Keys.ToImmutableHashSet();

        /// <inheritdoc />
        public INode<T> this[T value] => Contains(value) ? m_Nodes[value] : throw new ArgumentException("Value not in graph.");

        /// <summary>
        /// Adds the specified item to the graph.
        /// </summary>   
        /// <exception cref="ArgumentException">A node with the same value already exists in the graph.</exception>    
        [NotNull]
        public virtual Node<T> Add([NotNull] T item)
        {
            item.AssertNotNull(nameof(item));

            if (Contains(item))
            {
                throw new ArgumentException("A node with the same value already exists in the graph.");
            }

            var node = new Node<T>(this, item);
            m_Nodes.Add(item, node);
            m_NodesIds.Add(node.Id, node);

            OnNodeAdded?.Invoke(this, item);

            return node;
        }

        /// <summary>
        /// Tries to add the specified item to the graph.
        /// </summary>   
        /// <exception cref="ArgumentException">A node with the same value already exists in the graph.</exception>
        /// <returns>The added node, if it has been possible to add it, null otherwise.</returns>
        [CanBeNull]
        public virtual Node<T> TryAdd([NotNull] T item)
        {
            item.AssertNotNull(nameof(item));

            if (!Contains(item))
            {
                var node = new Node<T>(this, item);
                m_Nodes.Add(item, node);
                m_NodesIds.Add(node.Id, node);

                OnNodeAdded?.Invoke(this, item);

                return node;
            }

            return null;
        }

        /// <summary>
        /// Removes the specified item from the graph.
        /// </summary>
        /// <exception cref="ArgumentException">Item not in graph</exception>
        public virtual void Remove([NotNull] T item)
        {
            item.AssertNotNull(nameof(item));

            if (!Contains(item))
            {
                throw new ArgumentException("Item not in graph");
            }

            m_NodesIds.Remove(m_Nodes[item].Id);
            m_Nodes.Remove(item);

            OnNodeRemoved?.Invoke(this, item);
        }

        /// <summary>
        /// Gets the node with the specified value.
        /// </summary>
        /// <param name="value">The value of the searched node.</param>
        /// <exception cref="ArgumentException">Value not in graph.</exception>
        [NotNull]
        public Node<T> GetNode([NotNull] T value)
        {
            value.AssertNotNull(nameof(value));

            return Contains(value) ? m_Nodes[value] : throw new ArgumentException("Value not in graph.");
        }

        /// <summary>
        /// Tries the get the node with the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="node">The node.</param>
        [ContractAnnotation("=>true,node:NotNull; =>false,node:Null")]
        public bool TryGetNode([NotNull] T value, [CanBeNull] out Node<T> node)
        {
            value.AssertNotNull(nameof(value));

            if (Contains(value))
            {
                node = GetNode(value);
                return true;
            }

            node = null;
            return false;
        }

        /// <inheritdoc />
        public bool Contains(T value)
        {
            value.AssertNotNull(nameof(value));
            return m_Nodes.ContainsKey(value);
        }

        /// <inheritdoc />
        public bool Contains(Guid valueId)
        {
            return m_NodesIds.ContainsKey(valueId);
        }
        
        /// <inheritdoc />
        public bool HasCycles()
        {
            return m_Nodes.Values.Any(x => x.CyclesFound(ImmutableHashSet<Guid>.Empty));
        }

        /// <inheritdoc />
        public event EventHandler<T> OnNodeAdded;

        /// <inheritdoc />
        public event EventHandler<T> OnNodeRemoved;

        /// <summary>
        /// Merges the actual graph with the specified graph.
        /// <remarks>If otherGraph has connections to external graphs, these will be maintained.</remarks>
        /// <remarks>Connections in otherGraph can overwrite old connections in the actual graph
        /// if the two graphs have nodes with the same content.</remarks>
        /// </summary>
        /// <param name="otherGraph">The other graph.</param>
        public void Merge([NotNull] IGraph<T> otherGraph)
        {
            // adding nodes
            otherGraph.Values.ForEach(value => TryAdd(value));

            // adding connections
            otherGraph.Nodes.ForEach(node =>
            {
                node.Edges.Values.ForEach(edge =>
                {
                    INode<T> destination = edge.StartNode.Graph.Equals(edge.DestinationNode.Graph)
                        // if both node belong to the same graph, it has been added and will be in the actual graph.
                        ? GetNode(edge.DestinationNode.Value)
                        // if the destination belongs to another graph we simply restore the connection to the same node.
                        : edge.DestinationNode;
                    
                    GetNode(edge.StartNode.Value).AddOrModifyConnection(destination, edge.Weight);
                });
            });
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            return other is IGraph<T> graph  && graph.Id == Id;
        }
        
        /// <inheritdoc />
        public bool Equals(IGraph<T> other)
        {
            return other != null && other.Id == Id;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var result = "";
            foreach (var node in m_Nodes.Values)
            {
                result += node + "\n";
            }

            return result;
        }

        [NotNull]
        public string ToString([NotNull] Func<T, string> toString)
        {
            toString.AssertNotNull(nameof(toString));
            var result = "";
            foreach (var node in m_Nodes.Values)
            {
                result += toString(node.Value) + "\n";
            }

            return result;
        }

        [NotNull]
        public string ToStringExt([NotNull] Func<T, string> toString)
        {
            toString.AssertNotNull(nameof(toString));

            return Nodes.OrderBy(x => x.Value).Aggregate("", (current, node) => current + node.ToStringValues(toString) + "\n");
        }
    }
}
