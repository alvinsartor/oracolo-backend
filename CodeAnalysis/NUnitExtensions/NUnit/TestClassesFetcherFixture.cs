﻿using NUnit.Framework;

namespace CodeAnalysis.NUnitExtensions.NUnit
{
    [TestFixture]
    internal sealed class TestClassFetcherFixture
    {
        [Test]
        public void PathIsFetchedCorrectly()
        {
            string value = TestClassesFetcher.GetPathOfTestData($@"Whatever.cs");
            string expected =
                $@"C:\Users\a.sartor\source\repos\OrbWeaver\CodeAnalysis\NUnitExtensions\PLAYGROUND\Whatever.cs";
            
            Assert.That(value, Is.EqualTo(expected));           
        }
    }
}
