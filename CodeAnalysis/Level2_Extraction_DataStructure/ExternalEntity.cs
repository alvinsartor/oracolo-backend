﻿using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Level2_Extraction_DataStructure
{
    public sealed class ExternalEntity : IEntity
    {
        public ExternalEntity([NotNull] string fullName, [NotNull] string assembly, EntityType type)
        {
            Type = type;
            FullName = fullName.NotNull(nameof(fullName));
            Assembly = assembly.NotNull(nameof(fullName));
            Name = fullName.Substring(fullName.LastIndexOf('.'));
        }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public string FullName { get; }

        /// <inheritdoc />
        public string Assembly { get; }

        /// <inheritdoc />
        public EntityStatus Status => EntityStatus.LoadedAndExternal;

        /// <inheritdoc />
        public EntityType Type { get; }

        /// <inheritdoc />
        public bool Equals(IEntity other)
        {
            return other is ExternalEntity externalEntity
                   && externalEntity.Type == Type
                   && externalEntity.Assembly.Equals(Assembly)
                   && externalEntity.FullName.Equals(FullName);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is ExternalEntity other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Assembly.GetHashCode();
                hashCode = (hashCode * 397) ^ FullName.GetHashCode();
                return hashCode;
            }
        }
    }
}
