﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using NUnit.Framework;

namespace Extensions.NUnit
{
    [TestFixture]
    internal sealed class ObjectExtensionsFixture
    {
        [Test]
        public void EnumerateReturnsAnEnumeratorWithASingleElement()
        {
            const int element = 15;
            IEnumerable<int> enumerator = element.Enumerate();

            Assert.That(enumerator.First(), Is.EqualTo(element));
        }

        [Test]
        public void EnlistReturnsAnImmutableListWithASingleElement()
        {
            const int element = 15;
            ImmutableList<int> enumerator = element.Enlist();

            Assert.That(enumerator.Count, Is.EqualTo(1));
            Assert.That(enumerator.First(), Is.EqualTo(element));
        }

        [Test]
        public void ApplyCanBeUsedToExecuteAnAction()
        {
            const int element = 15;
            var copy = 0;
            element.Apply(x => copy += x);

            Assert.That(copy, Is.EqualTo(element));
        }

        [Test]
        public void ApplyCanBeUsedToExecuteAnFunctionWithoutArguments()
        {
            const int element = 15;
            int result = element.Apply(x => x * 2);

            Assert.That(result, Is.EqualTo(element * 2));
        }
    }
}
