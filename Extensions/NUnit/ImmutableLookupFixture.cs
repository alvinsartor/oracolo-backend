﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Extensions.NUnit
{
    [TestFixture]
    internal sealed class ImmutableLookupFixture
    {
        [Test]
        public void EmptyLookupDoesNotContainElements()
        {
            ImmutableLookup<string, string> emptyLookup = ImmutableLookup<string, string>.Empty;
            Assert.That(emptyLookup.Count, Is.EqualTo(0));
        }

        [Test]
        public void AddReturnsABrandNewCollection()
        {
            ImmutableLookup<string, string> initialLookup = ImmutableLookup<string, string>.Empty;
            ImmutableLookup<string, string> notEmptyLookup = initialLookup.Add("foo", "bar");

            Assert.That(initialLookup.Count, Is.EqualTo(0));
            Assert.That(notEmptyLookup.Count, Is.EqualTo(1));
            Assert.That(notEmptyLookup["foo"].SingleOrDefault(), Is.EqualTo("bar"));
            Assert.That(notEmptyLookup.Contains("foo"));
        }

        [Test]
        public void AddRangeReturnsABrandNewCollection()
        {
            ImmutableLookup<int, string> initialLookup = ImmutableLookup<int, string>.Empty;

            List<string> elements = new List<string>{"one", "two", "three", "four", "five", "six" };
            int GetKey(string element) => element.Length;

            ImmutableLookup<int, string> notEmptyLookup = initialLookup.AddRange(elements, GetKey);

            Assert.That(initialLookup.Count, Is.EqualTo(0));
            Assert.That(notEmptyLookup.Count, Is.EqualTo(3));
            Assert.That(notEmptyLookup.CountValues, Is.EqualTo(6));

            CollectionAssert.AreEquivalent(notEmptyLookup[3], new List<string> { "one", "two", "six" });
            CollectionAssert.AreEquivalent(notEmptyLookup[4], new List<string> { "four", "five", });
            CollectionAssert.AreEquivalent(notEmptyLookup[5], new List<string> { "three" });
        }

        [Test]
        public void AnErrorIsThrownWhenAddingTheMultipleElementsMultipleTimesWithTheSameKey()
        {
            ImmutableLookup<int, string> initialLookup = ImmutableLookup<int, string>.Empty;
            ImmutableLookup<int, string> notEmptyLookup = initialLookup.Add(3, "bar");

            Assert.DoesNotThrow(() => notEmptyLookup = notEmptyLookup.Add(3, "foo"));
            var exception = Assert.Throws<ArgumentException>(() => notEmptyLookup = notEmptyLookup.Add(3, "bar"));
            Assert.That(exception.Message.Contains("is already in the lookup"));
        }
    }
}
